import { CleanWebpackPlugin } from "clean-webpack-plugin";
import ESLintPlugin from "eslint-webpack-plugin";
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";
import HtmlWebpackPlugin from "html-webpack-plugin";
import path from "path";
import { Configuration } from "webpack";

const config:Configuration = {
    // specified the mode to be production. Webpack will automatically set process.env.NODE_ENV to "production"
    // which means we won't get the React development tools included in the bundle.
    mode: "production",
    entry: "./src/index.tsx",
    // The output field tells Webpack where to bundle our code. In our project, this is the build folder. We have used the [name]
    // token to allow Webpack to name the files if our app is code split. We have used the [contenthash] token so that the bundle
    // file name changes when its content changes, which will bust the browser cache.
    output: {
        path:path.resolve(__dirname, "build"),
        filename: '[name].[contenthash].js',
        publicPath: "",
    },
    module: {
        rules: [
            {
                test: /\.(ts|js)x?$/i,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets:[
                            "@babel/preset-env",
                            "@babel/preset-react",
                            "@babel/preset-typescript",
                        ],
                    },
                },
            },
        ],
    },
    resolve: {
		extensions:[".tsx", ".ts", ".js",".jsx"],
		alias: {
            "Utils": path.resolve(__dirname, "src/utils/"),
			"Application": path.resolve(__dirname, "src/app/"),
			"Common": path.resolve(__dirname, "src/common/"),
			"Model": path.resolve(__dirname, "src/model/"),
			"Components": path.resolve(__dirname, "src/common/components/"),
			"Actions": path.resolve(__dirname, "src/common/store/actions/"),
			"Reducers": path.resolve(__dirname, "src/common/store/reducers/"),
			"Types": path.resolve(__dirname, "src/common/types/"),
            "Services": path.resolve(__dirname,"src/common/service/"),

            "ExpressionAppComponents":path.resolve(__dirname, "src/expressionApp/components/"),
            "ExpressionAppModels":path.resolve(__dirname, "src/expressionApp/models/"),
            "ExpresssionAppActions":path.resolve(__dirname,"src/expressionApp/store/actions"),
			"ExpressionAppReducers":path.resolve(__dirname, "src/expressionApp/store/reducers"),
			"ExpressionAppTypes":path.resolve(__dirname, "src/expressionApp/store/types"),
			"ExpressionAppServices": path.resolve(__dirname, "src/expressionApp/services"),
            "ExpressionAppStore": path.resolve(__dirname, "src/expressionApp/store"),

            "OlapAppComponents":path.resolve(__dirname, "src/olapApp/components/"),
            "OlapAppModels":path.resolve(__dirname, "src/olapApp/model"),
            "OlapAppActions":path.resolve(__dirname, "src/olapApp/store/actions/"),
			"OlapAppReducers":path.resolve(__dirname, "src/olapApp/store/reducers/"),
			"OlapAppTypes":path.resolve(__dirname, "src/olapApp/store/types/"),
            "OlapAppStore":path.resolve(__dirname, "src/olapApp/store/"),

            "ModellerAppComponents":path.resolve(__dirname,"src/modellerApp/components/"),
			"ModellerAppModels":path.resolve(__dirname,"src/modellerApp/model/"),
			"ModellerAppActions":path.resolve(__dirname,"src/modellerApp/store/actions/"),
			"ModellerAppReducers":path.resolve(__dirname,"src/modellerApp/store/reducers/"),
			"ModellerAppTypes":path.resolve(__dirname,"src/modellerApp/store/types/"),
			"ModellerAppStore":path.resolve(__dirname,"src/modellerApp/store/"),
			"ModellerAppService":path.resolve(__dirname,"src/modellerApp/service/")	
		}
	},
    plugins: [
        new HtmlWebpackPlugin({
            template: "index.html",
        }),
        new ForkTsCheckerWebpackPlugin({
            async: false
        }), 
        new ESLintPlugin({
            extensions:["js", "jsx", "ts", "tsx"],
        }),
        new CleanWebpackPlugin()
    ],
};
export default config;