/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */

module.exports = {
  preset: 'ts-jest',
  collectCoverage: true,
  coverageDirectory: 'coverage',
  testEnvironment: 'jsdom',
  moduleDirectories:[
    "<rootDir>/src",
    "node_modules"
  ],
  //include:[
  //  "<rootDir>/src/**/*.ts",
  //  "<rootDir>/src/**/*.tsx"
  //],
  moduleNameMapper: {
   // "^/(.*)$": "../../src/$1",
    "^Common/(.*)$": "../../src/common/$1",
    "^Calendar/(.*)$": "../../src/components/calendar/$1",
    "^Model/(.*)$": "../../src/model/$1",
  },
  testPathIgnorePatterns:[
    "/node_modules/"
  ],
  globals: {
    'ts-jest': {
      tsconfig:false
    }
  },

  // This option sets the URL for the jsdom environment. It is reflected in properties such as location.href
   testEnvironmentOptions: {
    url: "http://localhost",
  },
  //testRegex: ".test.(ts|tsx)$",
  // An array of regexp pattern strings that are matched against all source file paths, matched files will skip transformation
  transformIgnorePatterns: ["/node_modules/(?!(konva)/)"],
  // The glob patterns Jest uses to detect test files
  testMatch: ["**/__tests__/**/*.[jt]s?(x)", "**/?(*.)+(spec|test).[tj]s?(x)"],

};