import autoprefixer from "autoprefixer";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import Dotenv from 'dotenv-webpack';
import ESLintPlugin from "eslint-webpack-plugin";
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";
import HtmlWebpackPlugin from "html-webpack-plugin";
import path from "path";
import { Configuration, HotModuleReplacementPlugin } from "webpack";

/**
The mode field tells Webpack whether the app needs to be blunded for production or development. We are configuring
Webpack for development, so we have set this to "devdevelopment". Webpack will automatically set process.env.NODE_ENV to 
"devdevelopment" which means we get the React development tools included in the bundle.

The output.public field tells webpack what the root path is in the app. This is important for deep linking in the dev server
to work properly.
The entry field tells webpack where to start looking for modules to bundle. In our project, this index.tsx
The module field tells Webpack how different modules will be treated. Our project is telling Webpack to use the babel-loader plugin to process files with .js, .ts and .tsx extensions.
The resolve.extensions fields tells Webpack what file types to look for in which order during module resolution. We need to tell it to look for Typescript files as well as javascript files.
The HtmlWebpackPlugin creates the HTML file.  We have told this to use our index.html in the src folder as the template.
The HotModuleReplacementPlugin and devServer.hot allow modules to be updated while an application is running, without a full reload.
The devtool fields tells Webpack to use full inline source maps. This allows us to debug the original code before transpilation.
The devServer field configures the Webpack development server. We tell webpack that the root of the webserver is the build folder, and to serve files on port 4000. 
historyApiFallback is necessary for deep links to work in multi-page apps. We are also telling Webpack to open the browser after the server has been started.
*/
const config: Configuration = {
	mode: "development",
	output: {
		publicPath: "/",
	},
	entry: "./src/index.tsx",
	module: {
		rules:[{
				test:/\.css$/i,
				use:["style-loader", "css-loader"]
			},
			{
				test:/\.(scss)$/,
				use: [{
					// Adds CSS to the DOM by injecting a `<style>` tag
					loader: 'style-loader'
				},{
					//Interprets `@import` and `url()` like `import/require()` and will resolve them
					loader: 'css-loader'
				},
				{
					//Loader for webpack to process css with PostCSS
					loader: 'postcss-loader',
					options: {
						postcssOptions: {
							plugins: () => [
								autoprefixer
							]
						}
					}
				}, {
					// Loads a SASS/SCSS file and compiles it to CSS
					loader: 'sass-loader'
				}
				]
			},
			{
				test: /\.(ts|js)x?$/i,
				exclude: /node_modules/,
				include: path.join(__dirname,"src"),
				use: {
					loader: "babel-loader",
					options: {
						presets:[
							"@babel/preset-env",
							"@babel/preset-react",
							"@babel/preset-typescript",
						],
					},
				},
			},
		],
	},
	resolve: {
		extensions:[".tsx", ".ts", ".js",".jsx"],
		alias: {
			"Utils": path.resolve(__dirname, "src/utils/"),
			"Application": path.resolve(__dirname, "src/app/"),
			"Common": path.resolve(__dirname, "src/common/"),
			"Model": path.resolve(__dirname, "src/common/model/"),
			"Components": path.resolve(__dirname, "src/common/components/"),
			
			
			"Actions": path.resolve(__dirname, "src/common/store/actions/"),
			"Reducers": path.resolve(__dirname, "src/common/store/reducers/"),
			"Types": path.resolve(__dirname, "src/common/types/"),
			"Services": path.resolve(__dirname, "src/common/service/"),
			
			"ExpressionAppComponents":path.resolve(__dirname, "src/expressionApp/components/"),
			"ExpressionAppModels":path.resolve(__dirname, "src/expressionApp/models/"),
			"ExpresssionAppActions":path.resolve(__dirname,"src/expressionApp/store/actions"),
			"ExpressionAppReducers":path.resolve(__dirname, "src/expressionApp/store/reducers"),
			"ExpressionAppTypes":path.resolve(__dirname, "src/expressionApp/store/types"),
			"ExpressionAppServices": path.resolve(__dirname, "src/expressionApp/services"),
			"ExpressionAppStore": path.resolve(__dirname, "src/expressionApp/store"),
			
			"OlapAppComponents":path.resolve(__dirname, "src/olapApp/components/"),
			"OlapAppModels":path.resolve(__dirname, "src/olapApp/model"),
			"OlapAppActions":path.resolve(__dirname, "src/olapApp/store/actions/"),
			"OlapAppReducers":path.resolve(__dirname, "src/olapApp/store/reducers/"),
			"OlapAppTypes":path.resolve(__dirname, "src/olapApp/store/types/"),
			"OlapAppStore":path.resolve(__dirname, "src/olapApp/store/"),

					
			"ModellerAppComponents":path.resolve(__dirname,"src/modellerApp/components/"),
			"ModellerAppModels":path.resolve(__dirname,"src/modellerApp/model/"),
			"ModellerAppActions":path.resolve(__dirname,"src/modellerApp/store/actions/"),
			"ModellerAppReducers":path.resolve(__dirname,"src/modellerApp/store/reducers/"),
			"ModellerAppTypes":path.resolve(__dirname,"src/modellerApp/store/types/"),
			"ModellerAppStore":path.resolve(__dirname,"src/modellerApp/store/"),
			"ModellerAppService":path.resolve(__dirname,"src/modellerApp/service/")		
		}
	},
	
	devtool: "inline-source-map",
	devServer: {
		static: path.join(__dirname, "build"),
		historyApiFallback: true,
		port: 4000, 
		open: true,
		hot: true,
		client: {
			overlay:false
		},
	},
	plugins: [
		// using .env file for configuration.
		new Dotenv({
			path: `./.env.development`,
			systemvars: true
		}),
		new HtmlWebpackPlugin({
			template: "index.html",
		}),
		new HotModuleReplacementPlugin(),
		// Adding type checking into the webpack process, Use the following plugin to handle type checking
		// during webpack process. Webpack will inform us of any type errors.
		new ForkTsCheckerWebpackPlugin({
			async: false
		}),
		// Adding linting during webpack process. This means that webpack will inform us of any linting errors.
		new ESLintPlugin({
			extensions:["js", "jsx", "ts", "tsx"]
		}),
		// this plugin will clear out the build folder at the start of the bundling process.
		new CleanWebpackPlugin(),
	],
};
export default config;
