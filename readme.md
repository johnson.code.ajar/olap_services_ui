Olap service ui.
The UI to build a olap model which is defined by dimensions, cube and rules.

lib: The standard typing to be included in the type checking process. In our case, we have chosen to use the types for the browser's DOM and the latest version of ECMAScript.

allowJs: Whether to allow javascript files to be compiled.
allowSyntheticDefaultImports: This allows default imports from modules with no default export in the type checking process.
skipLibCheck: Whether to skip type checking of all the type declaration files (*.d.ts)
esModuleInterop: This enables compatibility with Babel.
strict: This sets of level of type checking to very high, When this is true, the project is said to be running in script mode.
forceConsistentCasingInFileNames:Ensures that the casing of referenced file names is consistent during the type checking process.
moduleResolution:How module dependencies get resolved, which is node for our project.
resolveJsonModule: This allows modules to be in .json files which are useful for configuration files.
noEmit: Whether to support JSX in .tsx files.
include: These are the files and folders for Typescript to check. In our project, we have specified all the files in the src folder.

//Babel dependencies.
@babel/core : Core babel library
@babel/preset-env: This is a collection of plugins that allow us to use the latest javascript features but still target browsers that don't support them.
@babel/preset-react: This is a collection of plugins that enable Babel to transform React code into javascript.
@babel/preset-typescript: This is a plugin that enables Babel to transform Typescript code into javascript.
@babel/plugin-transform-runtime and @babel/runtime: These are plugins that allow us to use the async and await javascript features.

// Eslint
eslint: core eslint library
eslint-plugin-react: contain some standard linting rules for react code.
eslint-plugin-react-hooks: some linting rules for react hooks code.
@typescript-eslint/parser: Typescript code to be lint.
@typescript-eslint/eslint-plugin: This contains some standard linting rules for Typescript code.
We have configured ESLint to use the Typescript parser, and the standard React and Typescript rules as base set of rules. We have explicitly added the two React hook rules and suppresessed the react/prop-types ruel because prop types aren't relevant in React with Typescript projects. We have also told ESLint to detect the version of React we are using.

//Webpack.
Webpack plugin html-webpack-plugin which will generate the HTML. Let install this.

Require the .eslintrc.json configuration to parse modules in typescript
Require the .babelrc.json to parse webpack config file in typescript.


// Examples used in build this project.
https://github.com/wazooinc/spring-boot-frontend-demo


// graph diagram 
cytoscape
diagram-js
konva
mxgraph
react-digraph
react-graph-vis
react-konva
storm-react-diagrams
vis