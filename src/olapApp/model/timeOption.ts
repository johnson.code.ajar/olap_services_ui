import { Option, PeriodType, monthOptions, quarterOptions, startDayOptions, weekOptions, weekSpanOptions, yearOptions } from "olapApp/model/timeOptionType";

export class TimeOption {
  private options:Map<PeriodType, Option[]>;
  private periods:Map<PeriodType, boolean>;
  private forceMonthEnd:boolean;
  private startDate:Date;
  private switchOverDate:Date;
  public constructor() {
    this.options = new Map();
    this.periods = new Map()
    this.forceMonthEnd = false;
    this.startDate = new Date();
    this.switchOverDate = new Date();
  }

  public setStartDate(inDate:Date) {
    this.startDate = inDate;
  }

  public setSwitchOverDate(inDate:Date) {
    this.switchOverDate = inDate;
  }

  public setOption(period:PeriodType, option:Option|undefined) {
    if (option === undefined) {
      return
    }
    if (this.options.get(period)){
      this.options.get(period)?.push(option);
    }
    this.options.set(period, [option]);
  }

  public setPeriod(period:PeriodType, isSelected:boolean) {
    this.periods.set(period, isSelected);
  }

  public getOptions():Map<PeriodType, Option[]>{
    return this.options;
  }

  public getOption(period:PeriodType):Option[]|undefined {
    return this.options.get(period);
  }

  public getPeriod(period:PeriodType):boolean|undefined {
    return this.periods.get(period);
  }
  public getPeriods():Map<PeriodType, boolean>{
    return this.periods;
  }

  private isOptionSelected(type:PeriodType, option:Option):boolean {
    if(this.options.size === 0){
      return false;
    } else if (this.options.get(type)?.length === 0){
      return false;
    }
    this.options.get(type)?.forEach(soption=>{
      if (soption.id === option.id){
        return true;
      }
    })
    return false;
  }
  public setDefaultOption(period:PeriodType){
    let defaultOption:Option|undefined;
    if (period === PeriodType.YEAR) {
      this.setOption(period, yearOptions.default(period));
    } else if (period === PeriodType.QUARTER) {
      this.setOption(period, quarterOptions.default(period));
    } else if (period === PeriodType.MONTH) {
      if (this.periods.get(PeriodType.YEAR)|| this.periods.get(PeriodType.QUARTER)) {
          if (this.isOptionSelected(PeriodType.YEAR, yearOptions.options[1]) ||
              this.isOptionSelected(PeriodType.QUARTER, quarterOptions.options[1])){
              this.setOption(period, monthOptions.default(PeriodType.WEEK))
           } else if (this.isOptionSelected(PeriodType.YEAR, yearOptions.options[0]||
              this.isOptionSelected(PeriodType.QUARTER, quarterOptions.options[0]))) {
                this.setOption(period, monthOptions.default(PeriodType.MONTH));
           }
      }
    }else if (period === PeriodType.WEEK) {
        this.setOption(period, weekOptions.options[0]); // add no of day in a week.
        this.setOption(period, this.forceMonthEnd?weekSpanOptions.options[3]:weekSpanOptions.options[2]);
        this.setOption(period, startDayOptions.options[this.startDate.getDay()])
    }
  }

  public resetOptions() {
    this.options.clear();
    this.periods.clear();
    this.startDate = new Date();
    this.switchOverDate = new Date();
  }
}



