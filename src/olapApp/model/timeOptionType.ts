//import { getOptions } from "eslint-webpack-plugin/declarations/options";

export enum PeriodType {
  ALL="ALL",
  DAY="DAY",
  WEEK="WEEK",
  MONTH="MONTH",
  QUARTER="QUARTER",
  SEMESTER="SEMESTER",
  YEAR="YEAR"
}

export const py2:{[key:string]:{name:string, label:string}} = {
  "ALL":{name:"", label:""}
};
export const PeriodTypes = new Map<PeriodType, {name:string; label:string;}>([
  [PeriodType.ALL, {name:"", label:""}],
  [PeriodType.DAY, {name:"day", label:"D"}],
  [PeriodType.WEEK, {name:"week", label:"W"}],
  [PeriodType.MONTH, {name:"month", label:"M"}],
  [PeriodType.SEMESTER,{name:"semester", label:"S"}],
  [PeriodType.YEAR, {name: "year", label:"S"}]
]);


export interface TimeAttribute {
  period:Period;
  name:string;
  type:PeriodType;
  levelNo:number;
  nextPeriod:string;
  prevPeriod:string;
  firstPeriod:string;
  lastPeriod:string;
}

export enum YearOption {
  YEAR12MONTH="12MONTH",
  YEAR52WEEK="52WEEK"
}

export enum QuarterOption {
  QUARTER3MONTH="3MONTH",
  QUARTER13WEEK="13WEEK"
}

export enum SemesterOption {
  SEMESTER2="2SEM",
  SEMESTER3="3SEM"
}

export enum MonthOption {
  CALENDAR="CALENDAR",
  MONTH4WEEK="444",
  MONTH454WEEK="454",
  MONTH544WEEK="544",
  MONTH445WEEK="445"
}

export enum WeekOption {
  WEEK7DAY="7DAY"
}

export enum WeekSpanOption {
  FIRST="FIRST",
  SECOND="SECOND",
  BIGGEST="BIGGEST",
  SPLIT="SPLIT"
}

export enum StartDayOption {
  SUN="SUNDAY",
  MON="MONDAY",
  TUE="TUESDAY",
  WED="WEDNESDAY",
  THU="THURSDAY",
  FRI="FRIDAY",
  SAT="SATURDAY"
}

export interface Option {
  id:string;
  label:string;
  values: number[];
}

export interface LevelOptions {
  legend: string;
  options: Option[];
  default: (option:string)=>Option|undefined;
  option: (id:string)=>Option|undefined;
}

const getOption = (id:string, options:Option[]):Option|undefined=>{
    const foptions=options.filter(option=>option.id === id);
    return foptions?foptions[0]:undefined;
}
export const yearOptions:LevelOptions = {  
  legend:"Choose the kind of years you want:",
  options:[
    {id:YearOption.YEAR12MONTH, label:"Gregorian 365 or 366 days", values:[12]},
    {id:YearOption.YEAR52WEEK, label:"Lunar 52 weeks", values:[52]}
  ],
  default:(period:string):Option|undefined =>{
    if (period === PeriodType.MONTH) {
      return yearOptions.options[0];
    } else if (period === PeriodType.WEEK) {
      return yearOptions.options[1];
    }
    return yearOptions.options[0];
  },
  option:(id:string)=> getOption(id, yearOptions.options)
};

export const quarterOptions:LevelOptions={
  legend:"Choose the calendar type.",
  options:[ {id:QuarterOption.QUARTER3MONTH, label:"3 Months", values:[3]},
  {id:QuarterOption.QUARTER13WEEK, label:"13 Weeks", values:[13]}
  ],
  default:(period:string):Option|undefined => {
    if(period === PeriodType.MONTH) {
      return quarterOptions.options[0];
    } else if (period === PeriodType.WEEK) {
      return quarterOptions.options[1];
    }
    return quarterOptions.options[0];
  },
  option:(id:string)=>getOption(id, quarterOptions.options)
};

export const semesterOptions:LevelOptions = {
legend:"",
options:[
    {id:SemesterOption.SEMESTER2, label:"", values:[2]},
    {id:SemesterOption.SEMESTER3, label:"", values:[3]}
  ],
default:(period:string):Option|undefined => undefined,
option:(id:string)=>getOption(id, semesterOptions.options)
};

export const monthOptions:LevelOptions={
  legend:"Choose the type of month for thi dimension.",
  options:[
    {id:MonthOption.CALENDAR, label:"Calendar", values:[]},
    {id:MonthOption.MONTH4WEEK, label: "444 Weeks", values:[4,4,4]},
    {id:MonthOption.MONTH454WEEK, label: "454 Weeks",values:[4,5,4]},
    {id:MonthOption.MONTH544WEEK,  label: "544 Weeks", values:[5,4,4]},
    {id:MonthOption.MONTH445WEEK, label:"445 Weeks" ,values:[4,4,5]}
  ],
  default:(period:string):Option|undefined => {
    if (period === PeriodType.WEEK) {
      return monthOptions.options[2];
    }
    return monthOptions.options[0];
  },
  option:(id:string)=>getOption(id, monthOptions.options)
};

export const weekOptions:LevelOptions = {
  legend:"No of days in a week.",
  options:[
    {id:WeekOption.WEEK7DAY, label:"7 days", values:[6]}
  ],
  default:(period:string):Option|undefined => undefined,
  option:(id:string)=>getOption(id, weekOptions.options)
};

export const weekSpanOptions:LevelOptions = {
legend:"How should a week which spam a Month boundary be treated ?",
options:[
    {id:WeekSpanOption.FIRST,  label: "Include in first month.", values:[]},
    {id:WeekSpanOption.SECOND,  label: "Include in second month.", values:[]},
    {id:WeekSpanOption.BIGGEST,  label: "Include in month contains biggest part.",values:[]},
    {id:WeekSpanOption.SPLIT,  label: "Split between two months.",values:[]}
  ],
  default:(period:string):Option|undefined => undefined,
  option:(id:string)=>getOption(id, weekSpanOptions.options)
};

export const startDayOptions:LevelOptions = {
  legend:"Start day of the week.",
  options:[
    {id:StartDayOption.SUN,  label:"Sunday",values:[1]},
    {id:StartDayOption.MON,  label: "Monday",values:[2]},
    {id:StartDayOption.TUE,  label: "Tuesday", values:[3]},
    {id:StartDayOption.WED,  label: "Wednesday", values:[4]},
    {id:StartDayOption.THU,  label: "Thursday", values:[5]},
    {id:StartDayOption.FRI,  label: "Friday", values:[6]},
    {id:StartDayOption.SAT,  label: "Saturday",values:[7]},
  ],
  default:(period:string):Option|undefined => undefined,
  option:(id:string)=>getOption(id, startDayOptions.options)
};

export interface Period {
  startDate:Date;
  endDate:Date;
}

export interface FormatPattern {
  prefix:string;
  suffix:string;
  type:PeriodType;
  timePattern:string;
  datePattern:string;
  index:number;
  pattern:FormatPattern;
}