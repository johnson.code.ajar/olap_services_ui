import { ElementType } from "./plan";

export interface OlapTab {
  id: string;
  name: string;
  type: ElementType;
}