export enum ElementType {
  PLANS='plans',
  PLAN="plan",
  CUBES="cubes",
  CUBE="cube",
  DIMENSIONS="dimensions",
  DIMENSION="dimension",
  SCRIPTS="scripts",
  SCRIPT="script",
  VIEWS="views",
  VIEW="view"
}
export interface PlanNode {
  name: string;
  id: string;
  type: ElementType;
  isCollapsed: boolean;
  children?:PlanNode[]
}

export const initPlanTree: PlanNode = {
  name:'plan 1',
  id: '1',
  type: ElementType.PLAN,
  isCollapsed: true,
  children: [{
    name: ElementType.DIMENSIONS,
    type: ElementType.DIMENSIONS,
    id: '2',
    isCollapsed: true,
    children:[]
  },{
    name: ElementType.CUBES,
    type: ElementType.CUBES,
    id: '3',
    isCollapsed: true,
    children:[]
  }, {
    name: ElementType.SCRIPTS,
    id: '4',
    type: ElementType.SCRIPTS,
    isCollapsed: true,
    children:[]
  }]
}
export const initPlansTree:PlanNode = {
  name: ElementType.PLANS,
  type: ElementType.PLANS,
  id:'1',
  isCollapsed: true,
  children:[initPlanTree]
}


export const examplePlanTree:PlanNode={
  name: "Plans",
  id: '1',
  type: ElementType.PLANS,
  isCollapsed: true,
  children: [{
    name:'PlanA',
    id: '2',
    type:ElementType.PLAN,
    isCollapsed: true,
    children:[
      {
        name:'Dimensions',
        id: '3',
        type: ElementType.DIMENSIONS,
        isCollapsed: true,
        children:[{
          name:'dimA',
          id: '4',
          type:ElementType.DIMENSION,
          isCollapsed:true
        },{
          name:'dimB',
          id: '5',
          type: ElementType.DIMENSION,
          isCollapsed:true
        }]
      },{
        name:'Cubes',
        id: '6',
        type: ElementType.CUBES,
        isCollapsed: true,
        children:[{
          name: 'cubeA',
          id: '7',
          type: ElementType.CUBE,
          isCollapsed: true,
          children:[{
            name:'Views',
            id:'8',
            type: ElementType.VIEWS,
            isCollapsed: true,
            children:[{
              name: 'View1',
              id: '9',
              type: ElementType.VIEW,
              isCollapsed: true,
            }, {
              name: 'View2',
              id: '10',
              type: ElementType.VIEW,
              isCollapsed: true
            }]
          }]
        },{
          name: 'cubeB',
          id: '11',
          type: ElementType.CUBE,
          isCollapsed: true,
        }]
      }
    ]
  },{
    name: 'PlanB',
    id: '12',
    type: ElementType.PLAN,
    isCollapsed: true,
    children:[{
      name: 'Dimensions',
      id:'13',
      type: ElementType.DIMENSIONS,
      isCollapsed: true,
      children:[{
        name:'dimC',
        id: '14',
        type: ElementType.DIMENSION,
        isCollapsed: true,
      },{
        name: 'dimD',
        id: '15',
        type: ElementType.DIMENSION,
        isCollapsed: true,
      }]
    }]
  }]
}