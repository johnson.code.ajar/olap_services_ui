import NavigationBar from "Components/navigationBar/NavigationBar";
import SideBar from "Components/sidebar/sidebar";
import { OlapDashboard } from "OlapAppComponents/dashboard/olapDashboard";
import { OlapDashboardProvider } from "OlapAppComponents/dashboard/olapDashboardContext";
import OlapSideBarTabs from "OlapAppComponents/tabs/olapSideTabs";
import React, { useState } from "react";
import { BodyContainer, DashboardContainer, FooterContainer, WorkspaceContainer } from "../app/appStyle";
import '../scss/styles.scss';
import { OlapAppContainer } from "./olapApp.style";



const OlapApp = ({isSideBarOpen}:{isSideBarOpen:()=>boolean}) => {
  const [isOpen, setIsOpen]=useState(false);
  const toggleSidebar = ()=>{
    setIsOpen(!isOpen);
  }
  return(
      <OlapAppContainer>
        <NavigationBar title="Olap Matters"></NavigationBar>
          <BodyContainer>
          <OlapDashboardProvider>
            <SideBar isOpen={isSideBarOpen()}>
              <OlapSideBarTabs></OlapSideBarTabs>
            </SideBar>
            <WorkspaceContainer isOpen={!isSideBarOpen()}>
            <DashboardContainer>
              <OlapDashboard></OlapDashboard>
            </DashboardContainer>
              <FooterContainer>
                Footer
              </FooterContainer>
            </WorkspaceContainer>
          </OlapDashboardProvider>
        </BodyContainer>
      </OlapAppContainer>
);
};

export default OlapApp;

