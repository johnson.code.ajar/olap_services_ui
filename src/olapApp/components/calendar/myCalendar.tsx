import React, {useEffect, useState} from "react";
import styled, {css} from "styled-components";

const week = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"];
const days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const leapDays = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
const months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]

const CalendarContainer = styled.div`
    width:400px;
    border: 1px solid green;
`
const CalendarInputContainer = styled.div`
    display:flex;
    flex-direction:row;
    align-items: center;
`
const CalendarMenuContainer = styled.div`
    width:400px;
    position:fixed;
    border: 1px solid green;
    ${(props: TimeMenuProps)=> props.toggleMenu && css`
        display:none;
    `}
`


const CalendarHeader = styled.div`
    display: flex;
    flex-direction: row;
    padding:10px 10px 5px 10px;
    justify-content : space-between;
    align-items: center;
    border: 1px solid green;
` 
const CalendarBody = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    padding-left: 15px;
    // border: 1px solid green;
`

const CalendarButton = styled.div`
    display:flex;
    cursor: pointer;
    justify-content: center;
    align-items: center;
    // border: 1px solid green;
`

const HeaderLabel = styled.div`
    display:flex;
    align-items:center;
    justify-content:center;
    
`
const CalendarLabel = styled.label`
    width:13%;
    height: 40px;
    cursor:pointer;
    display:flex;
    align-items: center;
    justify-content: center;

    
    ${(props:DayProps)=> 
        props.isSelected && css`
            border: 1px solid #eee;
            background-color:green;
        `
    }
    ${(props:DayProps)=>props.isToday && css`
        border: 1px solid #eee;
        foreground-color:blue;
    `}
`

interface DayProps {
    isToday?:boolean;
    isSelected?:boolean;
}

interface TimeMenuProps {
    toggleMenu: boolean;
}

const MyCalendar = () => {
    const getStartDayOfMonth = (date: Date) => {
        return new Date(date.getFullYear(), date.getMonth(), 1).getDay();
    }
    const today = new Date();
    const [date, setDate] = useState(today)
    const [day, setDay] = useState(date.getDay());
    const [month, setMonth] = useState(date.getMonth());
    const [year, setYear] = useState(date.getFullYear());
    const [startDay, setStartDay] = useState(getStartDayOfMonth(date));
    const [hideMenu, setHideMenu] = useState(true);
    // apply the effect hook after each render if count changes.
    useEffect(()=> {
        setDay(date.getDate());
        setMonth(date.getMonth());
        setYear(date.getFullYear());
        setStartDay(getStartDayOfMonth(date))
    })
    
    const isLeapYear = (year:number) => {
        return (year %4 === 0 && year %100 !==0) || year % 400 ===0;
    }

    const days = isLeapYear(year) ? leapDays : leapDays;

    const dispWeeks = () =>{
        return (
            week.map(w => <CalendarLabel key={w}>{w}</CalendarLabel>)
        )
    };
    
    const increaseMonth = () => {
       setDate(new Date(year, month+1, day))
    }

    const decrementMonth = ()=> {
        setDate(new Date(year, month-1, day))
    }

    const selectDate = (year:number, month:number, day: number)=> {
        const selectedDate = new Date(year, month, day)
        setDate(selectedDate)
    }
    const dispDays = (date:Date) => {
        console.log(`Today date ${today.getDate()}`)
        return (Array(days[month]+startDay-1).fill(null).map((v, d) =>{
                d = d-startDay+2;
                return(<CalendarLabel key={d} 
                        isSelected={d===day}
                        isToday={d === today.getDate()}
                        onClick={()=>selectDate(year, month, d)}
                        >{d>0?d:''}
                    </CalendarLabel>)
            }));
    }

    const toggleMenu = () => {
        setHideMenu(!hideMenu)
    }
    /*
    const dispStartDay = () => {
        console.log(`start day: ${startDay}`)
        return (startDay>0?Array(startDay-1).fill(null).map((_,d)=>
        <CalendarLabel key={d} isSelected={d===day}></CalendarLabel>):(<></>));
    }
    const dispDays = (date:Date) => {
        const dayElements = Array(days[month]-(startDay+1)).fill(null).map((_,d)=> {
            const dd = d + (d===0 && startDay<=0 ? (startDay+2):1);
            return(<CalendarLabel key={d+startDay} isSelected={dd===day}>{dd}</CalendarLabel>)
        });
        return dayElements
    }*/
    return(<CalendarContainer>
        <CalendarInputContainer>
                <input style={{"flex":2, "height":"30px"}}type="text"/>
                <button style={{"height":"30px"}}  onClick={()=>toggleMenu()}>down</button>
        </CalendarInputContainer>
        <CalendarMenuContainer toggleMenu={hideMenu}>
            <CalendarHeader>
                <CalendarButton onClick={()=>decrementMonth()}>Prev</CalendarButton>
                <HeaderLabel>{months[month]} {year}</HeaderLabel>
                <CalendarButton onClick={()=>increaseMonth()}>Next</CalendarButton>
            </CalendarHeader>
            <CalendarBody>
                {dispWeeks()}
                {dispDays(date)}
            </CalendarBody>
        </CalendarMenuContainer>
    </CalendarContainer>)
}
export default MyCalendar;