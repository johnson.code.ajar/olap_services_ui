import { OlapTab } from "OlapAppModels/workspace";
import React, { Dispatch, createContext, useReducer } from "react";
interface OlapDashboardState {
  tabs:OlapTab[];
}

const initialDashboardState:OlapDashboardState = {
  tabs:[]
}
export enum OlapDashboardActionType {
   ADD_OLAP_TAB,
   REMOVE_OLAP_TAB
}

export type OlapDashboardActions = |{
  type: OlapDashboardActionType.ADD_OLAP_TAB,
  payload:{tab:OlapTab}
}|{
  type: OlapDashboardActionType.REMOVE_OLAP_TAB,
  payload:{tab:OlapTab}
};

export const olapDashboardReducers = (state:OlapDashboardState=initialDashboardState, action:OlapDashboardActions)=>{
  let partialState:OlapDashboardState;
  console.log("Inside Olap Dashboard reducers....");
  switch(action.type) {
    case OlapDashboardActionType.ADD_OLAP_TAB:
      const tabs = state.tabs;
      tabs.push(action.payload.tab);
      partialState = {
        tabs:tabs
      }
    break;
    case OlapDashboardActionType.REMOVE_OLAP_TAB:
      const filteredTabs = state.tabs.filter(tab=>action.payload.tab.id !== tab.id);
      partialState = {
        tabs: filteredTabs
      }
    break; 
  }
  return partialState ? {...state, ...partialState}:state;
}

interface OlapDashboardContextType {
  dashboardState: OlapDashboardState;
  dashboardDispatch: Dispatch<OlapDashboardActions>;
}

const  OlapDashboardContext = createContext<OlapDashboardContextType>({
  dashboardState: initialDashboardState,
  dashboardDispatch: ()=>null
});

const OlapDashboardProvider:React.FC<React.PropsWithChildren> = ({children}) =>{
  const [dashboardState, dashboardDispatch] = useReducer(olapDashboardReducers, initialDashboardState);
  return (<OlapDashboardContext.Provider value={{dashboardState, dashboardDispatch}}>{children}</OlapDashboardContext.Provider>)
};

export { OlapDashboardContext, OlapDashboardProvider };

