import { faClose } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { OlapTab } from "OlapAppModels/workspace";
import React, { useCallback, useContext } from "react";
import { Tab } from "react-bootstrap";
import Tabs from "react-bootstrap/Tabs";
import { OlapDashboardContainer } from "./olapDashboard.style";
import { OlapDashboardActionType, OlapDashboardContext } from "./olapDashboardContext";

const DashboardTabTitle = ({tab}:{tab:OlapTab})=>{
  const {dashboardState, dashboardDispatch} = useContext(OlapDashboardContext);
  const closeTab = useCallback(()=>{
    dashboardDispatch({
      type: OlapDashboardActionType.REMOVE_OLAP_TAB,
      payload:{tab:tab}
    });
  },[tab]);
  return<span>
    {tab.name}
    <a style={{paddingLeft:'10px'}} href='#' onClick={closeTab}><FontAwesomeIcon icon={faClose}></FontAwesomeIcon></a>
  </span>
};

export const OlapDashboard = ()=>{
  const {dashboardState, dashboardDispatch} = useContext(OlapDashboardContext);
  return (<OlapDashboardContainer>
    <Tabs>
      {
        dashboardState.tabs.map(tab=><Tab id={tab.id} key={tab.id} eventKey={`${tab.name}_${tab.id}`} title={<DashboardTabTitle tab={tab}></DashboardTabTitle>}>
          
         <div>{tab.name} Content</div>          
        </Tab>)
      }
    </Tabs>
  </OlapDashboardContainer>)
}

