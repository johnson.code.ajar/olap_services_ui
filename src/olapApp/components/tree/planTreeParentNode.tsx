import { faAngleDown, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { contentTreeActionCreators } from "OlapAppActions/contentTreeAction";
import { OlapDashboardActionType, OlapDashboardContext } from "OlapAppComponents/dashboard/olapDashboardContext";
import { PlanTreeChildNode } from "OlapAppComponents/tree/planTreeChildNode";
import { PlanNode } from "OlapAppModels/plan";
import React, { useCallback, useContext, useState } from "react";
import { useDispatch } from "react-redux";
import { PlanNodeName, PlanUL } from "./planContentTree.style";

interface Properties {
  node:PlanNode;
}

export const PlanTreeParentNode = ({node}:Properties) => {
  const key = `${node.type}_${node.id}`;
  const [isExpanded, setIsExpanded] = useState(false);
  const {dashboardState, dashboardDispatch} = useContext(OlapDashboardContext);
  const appDispatch = useDispatch();
  const selectNode = useCallback((e:React.MouseEvent, node:PlanNode)=>{   
    appDispatch(contentTreeActionCreators.activeContentTreeNode(node));
  },[]);

  const doubleClickAction:React.MouseEventHandler<HTMLUListElement> = useCallback((e)=>{
    setIsExpanded(!isExpanded);
   
  },[setIsExpanded, isExpanded]);

  const openNode:React.MouseEventHandler<HTMLLabelElement> = useCallback((e)=>{
    dashboardDispatch({
      type:OlapDashboardActionType.ADD_OLAP_TAB,
      payload:{ tab:{
        name: node.name,
        id: node.id,
        type: node.type
      }}
    });
  },[]);

  if(node.children && node.children.length>0) {
    return (
      <PlanUL key={key} id={key}  >
      <span key={key} onMouseEnter={(e)=>selectNode(e, node)}>
        <i style={{paddingRight:'5px'}} className="fas rotate" onDoubleClick={doubleClickAction}>
        {isExpanded?<FontAwesomeIcon icon={faAngleDown}></FontAwesomeIcon>:<FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon>}
        </i>
        <PlanNodeName onDoubleClick={openNode}>{node.name}</PlanNodeName>
       
        
      </span>
      {isExpanded && node.children.map((n,i)=><PlanTreeParentNode key={`${key}_${i}`} node={n}></PlanTreeParentNode>)}
    </PlanUL>);
  }
  return <PlanTreeChildNode node={node}></PlanTreeChildNode>
}