import { PlanContentTreeContext } from "OlapAppComponents/tree/planContentTreeContext";
import { PlanContentTreeMenu } from "OlapAppComponents/tree/planContentTreeMenu";
import { PlanTreeParentNode } from "OlapAppComponents/tree/planTreeParentNode";
import { PlanNode, initPlansTree } from "OlapAppModels/plan";
import { OlapAppState } from "OlapAppStore/index";
import React, { useContext } from "react";
import { connect } from "react-redux";


interface Properties {
  selectedNode: PlanNode;
}
export const PlanContentTree = ({selectedNode}:Properties)=>{
  const {state, } = useContext(PlanContentTreeContext); 
  return( 
    <div style={{width: '100%', display:'flex', flexDirection: 'column'}}>
    <PlanTreeParentNode node={state.plans}></PlanTreeParentNode>
    <PlanContentTreeMenu node={selectedNode || state.plans}></PlanContentTreeMenu>
    </div>
)};

const mapStateToProps = (state: OlapAppState) =>({
  selectedNode: state.contentTree? state.contentTree.selectedNode : initPlansTree
});


export default connect(mapStateToProps)(PlanContentTree);