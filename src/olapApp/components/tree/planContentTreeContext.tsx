import { PlanNode, examplePlanTree } from "OlapAppModels/plan";
import React, { Dispatch, createContext, useReducer } from 'react';
interface PlanContentTreeState {
  plans: PlanNode;
}

const initPlanTreeState: PlanContentTreeState = {
  plans: examplePlanTree
};

type PlanContentTreeActions = | {
  type: 'ADD_PLAN',
  payload: PlanNode
} ;


interface PlanContentTreeContextType {
  state: PlanContentTreeState;
  planTreeDispatch: Dispatch<PlanContentTreeActions>;
}

const planTreeReducers = (state: PlanContentTreeState, action: PlanContentTreeActions) =>{
  let partialState: PlanContentTreeState | undefined;
  switch(action.type) {
    case 'ADD_PLAN':
      const plans:PlanNode[]|undefined = state.plans.children;
      if(plans){
        plans.push(action.payload);
      }
      partialState = {
        plans:{
          ...state.plans,
          children:plans}
      }
    break;
    // default:
    //   throw new Error();
  }
  return partialState?{...state, partialState}:state;
}

const PlanContentTreeContext = createContext<PlanContentTreeContextType>({
  state: initPlanTreeState,
  planTreeDispatch: ()=>null
});

const PlanContentTreeProvider: React.FC<React.PropsWithChildren> = ({children}) =>{
  const [state, planTreeDispatch] = useReducer(planTreeReducers, initPlanTreeState);
  return(
    <PlanContentTreeContext.Provider value={{state, planTreeDispatch}}>
      {children}
    </PlanContentTreeContext.Provider>
  );
}

export { PlanContentTreeContext, PlanContentTreeProvider };
