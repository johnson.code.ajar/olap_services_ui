import styled from "styled-components";

export const PlanUL = styled.ul`
  display: flex;
  flex-direction: column;
  padding-inline-start: 5px;
  list-style-type:none;
  font-size:14px;
  width: 100%
  border: 1px solid green;
  // on hover span color change has to be define in parent and child
  :hover {
    &> span {
      color: #0d6efd;
    }
  }
  &> span {
    :hover {
      color: #0d6efd;
      font-size: 15px;
    }

  }
`
export const PlanLI = styled.li`
  display: flex;
  padding-inline-start: 20px;
  width: 100%;
  :hover {
    > span {
      color: #0d6efd;
      font-size: 15px;
    }
  }
`

export const PlanNodeName = styled.label`
  padding-left:2px;
`