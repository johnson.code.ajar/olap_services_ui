import { ContextMenu, MenuItemProp } from "Components/context-menu/contextMenu";
import { PlanContentTreeContext } from "OlapAppComponents/tree/planContentTreeContext";
import { ElementType, PlanNode, initPlanTree } from "OlapAppModels/plan";
import React, { useCallback, useContext } from 'react';
export const PlanContentTreeMenu = ({node}:{node:PlanNode})=>{
  const {state, planTreeDispatch} = useContext(PlanContentTreeContext);
  const createNewPlan = useCallback(()=>{
    const noPlans = state.plans.children?.length;
    const newPlan = {...initPlanTree,
    name: `plan ${noPlans?noPlans+1:1}`};
    planTreeDispatch({
      type: 'ADD_PLAN',
      payload: newPlan
    });
  },[state.plans]);
  
  const plansMenuItems = (node:PlanNode) =>{
    const menuItems = [{
      name: 'New Plan',
      action: createNewPlan
    },{
      name: 'Rename Plan',
      action: ()=>console.log("Renaming Plan")
    }];
    return menuItems;
  }

  const cubesMenuItems = (node:PlanNode) =>{
    const menuItems = [{
      name: 'New Cube',
      action: ()=> console.log("New Cube....")
    }, {
      name: 'Delete Cube',
      action: ()=> console.log("Deleting cube...")
    }];
    return menuItems;
  }

  const dimensionsMenuItems = (node:PlanNode) => {
    const menuItems = [{
      name: 'New Dimension',
      action: ()=> console.log("New Dimension....")
    }];
    return menuItems;
  }

  const planMenuItems = (node:PlanNode)=>{
     const menuItems = [
      { name: 'Create Cube',
        action: ()=> console.log("Create Cube...")
      }, {
        name: 'Create Dimension',
        action: ()=> console.log("Create Dimension")
      }, {
        name: 'Create Script',
        action: ()=>console.log("Create Script")
      }
    ];
    return menuItems;
  }

  const cubeMenuItems = (node:PlanNode)=>{
    const menuItems = [{
      name: 'Add Dimension',
      action: ()=> console.log("Adding Dimension...")
    }, {
      name: 'Create View',
      action: ()=> console.log("Creating view...")
    }, {
      name: 'Delete View',
      action: ()=>console.log("Deleting view....")
    }];
    return menuItems;
  }

  const viewsMenuItems = (node:PlanNode)=>{
    const menuItems=[{
      name: 'Create View',
      action: ()=>console.log("Creating View")
    }]
    return menuItems;
  }

  const viewMenuItems = (node:PlanNode)=>{
    const menuItems=[{
      name: 'Rename',
      action: ()=>console.log("Renaming view...")
    }, {
      name: 'Delete',
      action: ()=>console.log("Deleting view")
    }];
    return menuItems;
  }

  const dimensionMenuItems = (node:PlanNode)=>{
    const menuItems= [{
      name: 'Rename',
      action: ()=> console.log("Renaming Dimension...")
    }, {
      name: 'Create Subset',
      action: ()=>console.log("Creating subset")
    }];
    return menuItems;
  }

  const PlanContextMenu = ({node}:{node:PlanNode}):JSX.Element=> {
      let menuItems:MenuItemProp[] = []
      if(node.type === ElementType.PLANS){
        menuItems = plansMenuItems(node);
      } else if (node.type === ElementType.CUBES){
        menuItems = cubesMenuItems(node);
      } else if(node.type === ElementType.DIMENSIONS) {
        menuItems= dimensionsMenuItems(node);
      } else if(node.type === ElementType.VIEWS){
        menuItems =viewsMenuItems(node);
      } else if(node.type === ElementType.PLAN) {
        menuItems=planMenuItems(node);
      } else if(node.type === ElementType.CUBE) {
        menuItems=cubeMenuItems(node);
      } else if(node.type === ElementType.DIMENSION) {
        menuItems= dimensionMenuItems(node);
      } else if(node.type == ElementType.VIEW) {
        menuItems = viewMenuItems(node);
      }
      if(menuItems.length === 0) {
        return <></>;
      }
      return (<ContextMenu type={node.type} targetId={node.id} items={menuItems}></ContextMenu>);
  };
  
  
  return (<PlanContextMenu node={node}></PlanContextMenu>)
}