import { OlapDashboardActionType, OlapDashboardContext } from "OlapAppComponents/dashboard/olapDashboardContext";
import { PlanNode } from "OlapAppModels/plan";
import { contentTreeActionCreators } from "OlapAppStore/actions/contentTreeAction";
import React, { useCallback, useContext } from "react";
import { connect, useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { PlanLI, PlanNodeName } from "./planContentTree.style";

interface Properties {
  node: PlanNode;
}

export const PlanTreeChildNode = ({node}:Properties) => {
  const key = `${node.type}_${node.id}`;
  const appDispatch = useDispatch();
  const {dashboardState, dashboardDispatch} = useContext(OlapDashboardContext);
  const selectNode = useCallback((e: React.MouseEvent, node:PlanNode)=>{
   appDispatch(contentTreeActionCreators.activeContentTreeNode(node));
  },[]);

  const openNode = useCallback((e:React.MouseEvent<HTMLLabelElement>)=>{
    dashboardDispatch({
      type:OlapDashboardActionType.ADD_OLAP_TAB,
      payload:{ tab:{
        name: node.name,
        id: node.id,
        type: node.type
      }}
    });
  },[]);

  return (
    <PlanLI key={key} id={key} >
      <span key={key} onMouseEnter={(e)=>selectNode(e, node)}>
        <PlanNodeName onDoubleClick={openNode}>{node.name}</PlanNodeName>
      </span>
    </PlanLI>);
}

const mapDispatchToProps = (dispatch: Dispatch, actions: typeof contentTreeActionCreators)=>({
  activeContentTreeNode: (node:PlanNode)=>dispatch(actions.activeContentTreeNode(node))
})

connect(undefined, mapDispatchToProps)(PlanTreeChildNode);

