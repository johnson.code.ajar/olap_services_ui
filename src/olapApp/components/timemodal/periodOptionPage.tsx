import {
  LevelOptions,
  monthOptions,
  quarterOptions,
  startDayOptions,
  weekOptions,
  weekSpanOptions,
  yearOptions,
} from "olapApp/model/timeOptionType";
import React, { useCallback, useState } from "react";
import styled from "styled-components";
import { PageContainer, PageTitle } from "./commonStyle";


const LevelPageContainer = styled(PageContainer)`
    
`
const LevelContainer = styled.div`
    display: flex;
    flex-direction: row;
`
const LevelDivOptions = styled.div`
    border: 1px solid green;
    padding-left:20px;
    padding-top:10px;
    width:90%;
`

const Level = styled.div`
    width:10%;
    border: 1px solid blue;
    padding-bottom: 10px;
    padding-top: 10px;
    padding-left: 25px;
    padding-right: 25px;
`
const EachLevel = styled.div`
    display:flex;
    gap:5px;
    flex-direction: row;
    padding-bottom: 20px;
`


enum timeLevels  {
    YEAR = "Year",
    QUARTER = "Quarter",
    MONTH = "Month",
    WEEK = "Week",
    DAY = "Day"
}


const LevelPage = ()=> {
    const [levelSelected, setLevelSelected] = useState("Year")
    const [levelOption, setLevelOption] = useState("Level Option")
    const levelMouseEnterAction = useCallback((level:string)=>{
        setLevelSelected(level)
    },
    [setLevelSelected])

    const levelOptionMouseEnterAction = useCallback((level:string)=>{
        console.log("Level Option Empty function....")
    },[])

    const displayOption = (level:string, levelOptions:LevelOptions) => {
        return(<fieldset id={level}>
            <legend>{levelOptions.legend}</legend>
            {
                levelOptions.options.map((option)=>{
                    return(
                        <div key={option.id}>
                            <input key={`radio_${option.id}`} type="radio" name={level}/> {option.label}
                        </div>)
                })
            }
            </fieldset>)
    }

    const levelSelector = ()=>{
        return Object.values(timeLevels).map((level:string)=>{
            return(
                <EachLevel key={level} onMouseEnter={()=>levelMouseEnterAction(level)}>
                    <input key={`check_${level}`} type="checkbox"  name={level} value={level}/>
                    <label>{level}</label>
                </EachLevel>
            )
        })
    }
    
    const displayYearOptions = () => displayOption("year", yearOptions);
    const displayQuarterOptions = () => displayOption("quarter", quarterOptions);
    const displayMonthOptions = () => displayOption("month", monthOptions);
    const displayWeekOptions = ()=> {
        return (<div>
                    {displayOption("week", weekSpanOptions)}
                    {displayOption("weekDay", startDayOptions)}
                </div>);
    };
    
    const displayStartDayOptions = () => {
        return (<>
            <div>{displayOption("noDays", weekOptions)}</div>
        </>)
    }

    const dispLevelOptions = (level:string)=>{
        if (levelSelected === "Year"){
            return displayYearOptions();
        } else if (levelSelected === "Quarter") {
            return displayQuarterOptions();
        } else if (levelSelected === "Month") {
            return displayMonthOptions();
        } else if (levelSelected === "Week") {
            return displayWeekOptions();
        } else if (levelSelected === "Day"){
            return displayStartDayOptions();
        }
    }

    return (<LevelPageContainer>
            <PageTitle>Period Level</PageTitle>
            <LevelContainer>
                <Level>
                    {levelSelector()}
                </Level>
                <LevelDivOptions onMouseEnter={()=>levelOptionMouseEnterAction(levelSelected)} >{dispLevelOptions(levelSelected)}</LevelDivOptions>
            </LevelContainer>
        </LevelPageContainer>);
};

export default LevelPage;