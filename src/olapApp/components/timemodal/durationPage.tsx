import MyCalendar from "OlapAppComponents/calendar/myCalendar";
import { SimpleCalendar } from "OlapAppComponents/calendar/simpleCalendar";
import React from "react";
import { PageContainer, PageTitle } from "./commonStyle";
// import "./calendar/calendar.css";

const DurationPage = () => {
    const onPanelChange = (value:any, mode:any)=> {
        console.log(value, mode);
    }
    return(<>
        <PageContainer>
            <PageTitle>
                Duration
            </PageTitle>
            <div>
                Start Period:
                <SimpleCalendar></SimpleCalendar>
                End Period:
                <MyCalendar></MyCalendar>
            </div>
        </PageContainer>
        
    </>);
}

export default DurationPage;