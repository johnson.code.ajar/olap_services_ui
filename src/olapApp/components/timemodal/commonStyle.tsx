import styled from "styled-components"

export const PageTitle = styled.h1`
    flex-grow:1;
    font-size: 1em;
    text-align: center;
    color: palevioletred;
`

export const PageContainer = styled.section`
    display: flex;
    flex-direction:column;
    justify-content:left;
    border: 1px solid red;
`