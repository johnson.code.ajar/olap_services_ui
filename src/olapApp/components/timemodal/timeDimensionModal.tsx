import React, {useState, useCallback} from 'react';
import styled from "styled-components";
import LevelPage from './periodOptionPage';
import MemberPage from './memberPage';
import DurationPage from './durationPage';

const TimeWrapper = styled.section`
    display: flex;
    flex-direction: row;
    border: 1px solid blue;
    flex:2;
`

const LinkContainer = styled.div`
    display:flex;
    gap:5px;
    flex-direction: column;
    border: 1px solid green;
    flex: 0 0 150px;
    flex-item:1
`
const PageLink = styled.a`
   padding-top:2px;
   padding-bottom:2px;
   background-color:green;
   border: 1px solid green;
   &:hover {
       background-color: red;
   }
   flex-item:2
`

const TimePageWrapper = styled.div`
   flex: 1;
` 

const enum PageType{ PERIOD, DURATION, MEMBER}


interface PageName {
    type:PageType;
    value:string;
}

const pageNames:PageName[]=[
    {type: PageType.PERIOD , value: "Period Level"},
    {type: PageType.DURATION, value: "Duration"},
    {type: PageType.MEMBER, value: "Member Names"}
]

const TimeDimensionModal = ()=> {
    const [pageSelected, setSelectedPage] = useState(pageNames[1]);
    
    const pageClick = useCallback((event:React.MouseEvent<HTMLAnchorElement, MouseEvent>, pageName:PageName)=>{
        setSelectedPage(pageName)
    },[setSelectedPage])

    const getPageName = (pageName: PageName) => {
        return pageName.value
    }
    const pageLinks = pageNames.map((pageName:PageName)=>{
        return <PageLink key={pageName.value} onClick={(e)=>pageClick(e, pageName)}>{getPageName(pageName)}</PageLink>
    })

    const displayPage = (pageName: PageName)=>{
        
        if (pageName.type === PageType.MEMBER){
            console.log(pageName.type)
            return <MemberPage/>
        } else if (pageName.type === PageType.DURATION) {
            console.log(pageName.type)
            return <DurationPage/>
        } else if (pageName.type === PageType.PERIOD) {
            console.log(pageName.type)
            return <LevelPage/>
        }
    }

    return(<TimeWrapper>
                
                <LinkContainer>
                    {pageLinks}
                </LinkContainer>
                
                <TimePageWrapper>
                    {displayPage(pageSelected)}
                </TimePageWrapper>
               
             </TimeWrapper>)
       
    
}
export default TimeDimensionModal