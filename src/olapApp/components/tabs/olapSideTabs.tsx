import { OlapNavItem, OlapSideBarContent, OlapSideBarTabsContainer } from "OlapAppComponents/tabs/olapSideTabs.style";
import PlanContentTree from "OlapAppComponents/tree/planContentTree";
import { PlanContentTreeProvider } from "OlapAppComponents/tree/planContentTreeContext";
import React from "react";
import { Nav, Tab } from "react-bootstrap";


const OlapSideBarTabs = () => {
  return(<Tab.Container>
        <OlapSideBarContent>
          <Tab.Pane eventKey="Plans">
            <PlanContentTreeProvider>
              <PlanContentTree></PlanContentTree>
            </PlanContentTreeProvider>
          </Tab.Pane>
          <Tab.Pane eventKey="Dashboards">
            <div>Dashboards</div>
          </Tab.Pane>
          <Tab.Pane eventKey="Analysis">
            <div>Analysis</div>
          </Tab.Pane>
        </OlapSideBarContent>
      <OlapSideBarTabsContainer variant="pills" defaultActiveKey="Plans">
        <OlapNavItem><Nav.Link eventKey="Plans">Models</Nav.Link></OlapNavItem>
        <OlapNavItem><Nav.Link eventKey="Dashboards">Dashboards</Nav.Link></OlapNavItem>
        <OlapNavItem><Nav.Link eventKey="Analysis">Analysis</Nav.Link></OlapNavItem>
      </OlapSideBarTabsContainer>
    </Tab.Container>
  );
};

export default OlapSideBarTabs;