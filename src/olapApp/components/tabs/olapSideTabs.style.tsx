import { Nav, NavItem, Tab } from "react-bootstrap";
import styled from "styled-components";

export const OlapNavItem = styled(NavItem)`
  display: flex;
  width: 100%;
  text-align: center;
  padding: 0px;
  background: rgb(232, 245, 253);
  .nav-link {
    width: 100%;
    height: 100%;
    margin: 0 auto;
  }
`

export const OlapSideBarContent = styled(Tab.Content)`
width: 100%;
height: 100%;
display: flex;
flex: 90%;
.tab-pane {
  width:100%;
}
`

export const OlapSideBarTabsContainer = styled(Nav)`
display: flex;
height: 100%;
width: 100%;
flex: 1%;
flex-direction: row;
border: 1px solid green;
align-items: top;
`