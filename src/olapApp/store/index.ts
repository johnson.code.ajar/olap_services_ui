import { configureStore } from "@reduxjs/toolkit";
import { ExpressionAppActions } from "ExpressionAppActions/index";
import { combineReducers, Dispatch as ReduxDispatch } from "redux";
import logger from "redux-logger";
import { OlapAppActions } from "./actions/index";
import { contentTreeReducers } from "./reducers/contentTreeReducers";
import { sideBarReducers } from "./reducers/sidebarReducer";
import { ContentTreeState, SidebarState } from "./types/olapType";

export interface AppState {
  sidebar: SidebarState;
}

export interface OlapAppState {
  sidebar: SidebarState;
  contentTree: ContentTreeState;
}


const olapAppReducers = combineReducers({
  sidebar:sideBarReducers,
  contentTree:contentTreeReducers,
});

const websocketAppReducers = combineReducers({
  sidebar: sideBarReducers
});

export type expressionAppDispatch = ReduxDispatch<ExpressionAppActions>;

export type OlapAppDispatch = ReduxDispatch<OlapAppActions>;


export const olapAppStore = configureStore({
  reducer: olapAppReducers,
  devTools: process.env.NODE_ENV === "development",
  middleware: (getDefaultMiddleware)=> getDefaultMiddleware().concat(logger)
})

export const websocketAppStore = configureStore({reducer: websocketAppReducers,
  devTools: process.env.NODE_ENV === "development",
  middleware: (getDefaultMiddleware)=> getDefaultMiddleware().concat(logger)
})

