import { ElementType } from "OlapAppModels/plan";
import { ACTIVE_CONTENT_TREE_NODE } from "../actions/actionTypes";
import { contentTreeActions } from "../actions/contentTreeAction";
import { ContentTreeState } from "../types/olapType";
const initialState: ContentTreeState = {
  selectedNode: {name:'Plans',id:'Plans',type:ElementType.PLANS, isCollapsed:false}
}

export const contentTreeReducers = (state:ContentTreeState = initialState, action: contentTreeActions) => {
  let partialState: ContentTreeState | undefined;
  switch(action.type) {
    case ACTIVE_CONTENT_TREE_NODE:
      partialState={
        selectedNode: action.payload.node
      }
    break;
  }
  return (partialState?{...state, ...partialState}:state);
}