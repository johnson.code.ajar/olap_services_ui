
import { TOGGLE_SIDEBAR } from "../actions/actionTypes";
import { sidebarActions } from "../actions/sidebarAction";
import { SidebarState } from "../types/olapType";

const initialState:SidebarState = {
  isOpen:true
};
export const sideBarReducers = (state:SidebarState = initialState, action:sidebarActions):SidebarState=> {
  let partialState:SidebarState|undefined;
  switch(action.type) {
    case TOGGLE_SIDEBAR:
      partialState = {
        ...state,
        isOpen: action.payload.isOpen
      }
    break;
  }
  return (!partialState? state: {...state, ...partialState});
}