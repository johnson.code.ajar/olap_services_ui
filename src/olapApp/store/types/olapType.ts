import { PlanNode } from "OlapAppModels/plan";

export type SidebarState = {
  isOpen: boolean;
};

export type ContentTreeState = Readonly<{
  selectedNode: PlanNode;
}>;

export type OlapAppState = Readonly<{
  contentTree: ContentTreeState;
}>;
