import { ACTIVE_CONTENT_TREE_NODE } from "OlapAppActions/actionTypes";
import { PlanNode } from "OlapAppModels/plan";
import { Action } from "redux";

interface activeContentTreeNodeAction extends Action {
  type: typeof ACTIVE_CONTENT_TREE_NODE,
  payload: {node:PlanNode}
}

export type contentTreeActions = activeContentTreeNodeAction;

export const contentTreeActionCreators = {
  activeContentTreeNode:(node:PlanNode)=>({
    type: ACTIVE_CONTENT_TREE_NODE,
    payload:{node}
  })
}