import { contentTreeActionCreators, contentTreeActions } from "./contentTreeAction";
import { sidebarActionCreators, sidebarActions } from "./sidebarAction";

export const actionCreators = {
  sidebar: sidebarActionCreators,
  contentTree: contentTreeActionCreators
}



export type OlapAppActions = sidebarActions | contentTreeActions;

// export type ContentTreeActions = contentTreeActions;

export type WebsocketAppActions = sidebarActions;
