import { TOGGLE_SIDEBAR } from "olapApp/store/actions/actionTypes";
// This is no longer required...
import { Action } from "redux";
interface toggleSidebarAction extends Action {
  type: typeof TOGGLE_SIDEBAR;
  payload: {toggle:boolean};
}

//export type sidebarActions = | toggleSidebarAction;
export type sidebarActions = | {
  type: typeof TOGGLE_SIDEBAR,
  payload: {isOpen:boolean}
}

export const sidebarActionCreators = {
  toggleSideBar: (isOpen:boolean)=>({
    type: TOGGLE_SIDEBAR, 
    payload: {isOpen}
  })
}