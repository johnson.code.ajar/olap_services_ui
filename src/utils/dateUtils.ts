import moment from "moment";
const formatDate = (dateValue:Date)=> {
  return moment(dateValue).format("YYYY/MM/DD kk:mm:ss");
} 

export default formatDate;