import React from "react";
import Tabs from "react-bootstrap/Tabs";
export const FooterTabs = React.forwardRef(({children}:{children: React.ReactElement[]}, ref)=>{
  return <Tabs>
    {children.map(c=>c)}
  </Tabs>
});
FooterTabs.displayName = "AppFooterTabs";