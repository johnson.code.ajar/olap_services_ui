import styled, { css } from "styled-components";
export const ContextMenuContainer = styled.div<{top:number; left:number}>`
  position: absolute;
  display: flex;
  flex-direction: column;
  width:120px;
  border: 1px solid #ffffff2d;
  border-radius: 4px;
  padding: 5px;
  margin: 5px 5px;
  box-sizing: border-box;
  background-color: #0080ff;
  ${({ top, left }) => css`
  top: ${top}px;
  left: ${left}px;
  `}
`;

export const MenuItem = styled.a`
color: black;
text-decoration:none;
font-size: 12px;
:hover {
  color: white;
}
`