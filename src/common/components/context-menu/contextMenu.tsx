import React, { useCallback, useEffect, useLayoutEffect, useMemo, useRef, useState } from "react";
import { ContextMenuContainer, MenuItem } from "./contextMenu.style";
export interface MenuItemProp {
  name:string;
  action:React.MouseEventHandler<HTMLAnchorElement|HTMLLabelElement>;
}

interface ContextMenuData {
  x:number;
  y:number;
  visibility: boolean;
}
export const ContextMenu = ({type, targetId, items}:{type:string, targetId:string, items?:MenuItemProp[]})=>{
  const [data, setData] = useState<ContextMenuData>({
    x:0,
    y:0,
    visibility:false
  });
  const menuRef = useRef<HTMLDivElement>(null);

  useEffect(()=>{
    if(!items || items?.length==0) {
      setData({
        ...data, 
        visibility: false
      });
    }
  },[]);

  useLayoutEffect(()=>{
    if(!menuRef || !menuRef.current) {
      return;
    }
    if(data.x + menuRef.current.offsetWidth > window.innerWidth) {
      setData({
        ...data,
        x: data.x - menuRef.current.offsetWidth
      })
    }
    if(data.y + menuRef.current.offsetHeight > window.innerHeight) {
      setData({
        ...data,
        y: data.y - menuRef.current.offsetHeight
      });
    }
  },[data]);

  useEffect(()=>{
    const contextMenuEventHandler = (event:MouseEvent)=>{
      const targetElement= document.getElementById(`${type}_${targetId}`);
      if(!(event.target instanceof HTMLElement)) {
        return;
      }

      console.log(`menuRef=${menuRef?.current?.id} target=${targetElement?.id}`)
 
      if(targetElement && targetElement.contains(event.target) && !data.visibility) {
        event.preventDefault();
        setData({
          ...data,
          x: event.clientX,
          y: event.clientY,
          visibility: true
        })
      } else if(menuRef.current && !menuRef?.current.contains(event.target)) {
        setData({
          ...data,
          visibility:false
        })
      }
    }
    const offClickHandler = (event:MouseEvent) =>{
      setData({
        ...data,
        x: event.pageX,
        y: event.pageY,
        visibility: false
      })
    }
    
    window.addEventListener("contextmenu", contextMenuEventHandler);
    window.addEventListener("click", offClickHandler);
   
    return ()=>{
      window.removeEventListener("contextmenu", contextMenuEventHandler);
      window.removeEventListener("click", offClickHandler);
    }
  },[data, setData]);
  
  const mouseLeaveEventHandler = useCallback((event:React.MouseEvent<HTMLDivElement>) => {
      setData({
        ...data,
        x:0,
        y:0,
        visibility: false
      })
  },[data, setData])

  const contextMenu = useMemo(()=>{
    if(data.visibility) {
      const menuItems = items && items.map(item=><MenuItem key={`menuItem_${item.name}`} onClick={item.action}>{item.name}</MenuItem>);
      if(!menuItems || menuItems.length === 0) {
        return <></>;
      }
      return <ContextMenuContainer ref={menuRef} left={data.x} top={data.y} onMouseLeave={mouseLeaveEventHandler}>
      {menuItems}
     </ContextMenuContainer>
    }
    return <></>
  },[data.visibility])

 return (<>{contextMenu}</>);
};