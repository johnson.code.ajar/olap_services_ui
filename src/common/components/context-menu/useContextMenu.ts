import { useEffect, useState } from "react";
export const useContextMenu = ()=>{
  const [isMenuVisible, setIsMenuVisible] = useState(false);
  const [menuPosition, setMenuPosition] = useState({x:0, y:0});
  // This is redundant and currently not used in the useEffect.
  useEffect(()=>{
    const handleMenuVisibility = ()=>setIsMenuVisible(false);
    window.addEventListener("click", handleMenuVisibility);
    return ()=>{
      window.removeEventListener("click", handleMenuVisibility);
    }
  },[]);
  const menuOptions = (nodeType:string)=>{
    if(nodeType === 'ExpressionGroup') {
      return ["Open","Rename","Add Function", "Delete Function"]
    } else if(nodeType === "Expression") {
      return ["Open", "Rename", "Delete"]
    }
    return [];
  }
  return {
    isMenuVisible,
    setIsMenuVisible,
    menuPosition,
    setMenuPosition,
    menuOptions
  }
}