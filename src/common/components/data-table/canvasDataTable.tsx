import { CellProps } from "Components/canvas-grid/Cell";
import Grid, { CellInterface, GridRef, RendererProps, ScrollCoords } from "Components/canvas-grid/Grid";
import { DataTable } from "Model/data";
import { KonvaEventObject } from "konva/lib/Node";
import { RectConfig } from "konva/lib/shapes/Rect";
import { Vector2d } from "konva/lib/types";
import React, { useCallback, useRef, useState } from "react";
import { Group, Rect, Text } from "react-konva";
import { useMeasure } from "react-use";

interface CanvasDataTableProps {
  data: DataTable;
}
type ColumnWidthMap = {[key:number]:number}
// Use full width and height grid...
export const CanvasDataTable:React.FC<CanvasDataTableProps>  = (props:CanvasDataTableProps)=>{
  const [containerRef,{width, height}] = useMeasure<HTMLDivElement>();
  const headerRef = useRef<GridRef>(null);
  const bodyRef = useRef<GridRef>(null);
  const rowHeaderRef = useRef<GridRef>(null);
  const [columnWidthMap, setColumnWidthMap] = useState<ColumnWidthMap>({});
  const getCellValue = useCallback((cell:CellInterface) => `${props.data.columns[cell.columnIndex].values[cell.rowIndex]}`,[props.data]);
  const dragHandleWidth = 5;
  const frozenColumns = 0;
  // onMouseMove
  const DraggableRect = (dragProps:RectConfig)=>{
    return (<Rect
    fill = "blue"
    draggable
    hitStrokeWidth={20}
    onMouseEnter={()=>(document.body.style.cursor= "ew-resize")}
    onMouseLeave={()=>(document.body.style.cursor= "default")}
    dragBoundFunc = {(pos:Vector2d)=>{
      console.log(pos);
      return {...pos, y:0}
    }}
    {...dragProps}
    >
    </Rect>)
  }

  interface HeaderComponentProps extends RendererProps {
    // rowIndex: number,
    // columnIndex: number,
    //x?:number;
    // y:number,
    //width?: number;
    // height: number,
    // frozenColumns:number,
    //onResize:(columnIndex:number, newWidth:number)=>void
  }

  const handleResize = useCallback((columnIndex:number, newWidth:number) => {
    setColumnWidthMap({
        ...columnWidthMap,
        [columnIndex]:newWidth
      });

    headerRef.current?.resizeColumns([columnIndex], true);
    bodyRef.current?.resizeColumns([columnIndex], true);
  },[columnWidthMap]);
  const HeaderComponent = ({rowIndex, columnIndex, x,y,width,height, frozenColumns, onResize}:HeaderComponentProps)=>{
    const text = props.data.columns[columnIndex].name;
    const fill = "#eee";
    if(x === undefined || width === undefined ) {
      return <></>;
    }
    return (
      <Group>
        <Rect x={x} y={y} height={height} width={width} fill={fill} stroke="grey" strokeWidth={0.5}></Rect>
        <Text x ={x} y={y} height={height} width={width} text={text} fontStyle="bold" verticalAlign="middle" align="center"></Text>
        <DraggableRect x= {x + width - dragHandleWidth} y={y} width={dragHandleWidth} height={height} 
        onDragMove={(e:KonvaEventObject<DragEvent>)=>{
          const node = e.target;
          //if(!node)  return;
          const newWidth = node.x() - x + dragHandleWidth;
         
          onResize(columnIndex, newWidth)
        }}></DraggableRect>
      </Group>
    );
  }

  const RowHeaderComponent = ({rowIndex, columnIndex, x,y,width,height, frozenColumns, onResize}:HeaderComponentProps)=>{
    const text = `${rowIndex}`;
    const fill = "#eee";
    return (
      <Group>
        <Rect x={x} y={y} height={height} width={width} fill={fill} stroke="grey" strokeWidth={0.5}></Rect>
        <Text x ={x} y={y} height={height} width={width} text={text} fontStyle="bold" verticalAlign="middle" align="center"></Text>
       
      </Group>
    );
  }

  // const Cell = ({
  //   rowIndex,
  //   columnIndex,
  //   x,
  //   y,
  //   width,
  //   height,
  //   key,
  // }: CellProps) 
  const Cell  = (cellProps:CellProps)=> {
    //const text = `${rowIndex}x${columnIndex}`;
    const text = getCellValue(cellProps);
    const {x,y,key, width, height, rowIndex, columnIndex} = cellProps;
    const fill = "white";
    return (
      <React.Fragment key={key}>
        <Rect
          x={x}
          y={y}
          height={height}
          width={width}
          fill={fill}
          stroke="grey"
          strokeWidth={0.5}
        />
        <Text
          x={x}
          y={y}
          height={height}
          width={width}
          text={text}
          fontStyle="normal"
          verticalAlign="middle"
          align="center"
        />
      </React.Fragment>
    );
  };

  // padding bottom should be same as header grid height
  return(<div style={{display:'flex',flexDirection:'column',flex:1, width:'100%', height:'100%', paddingBottom:'20px'}} ref={containerRef} >

    <Grid
    columnCount = {props.data.columns.length}
    rowCount = {1}
    height = {20}
    frozenColumns={frozenColumns}
    ref={headerRef}
    width={width}
    columnWidth = {(index:number)=>{
      if (index in columnWidthMap) return columnWidthMap[index];
      return 100;
    }}
    rowHeight = {(index)=>{
      return 20;
    }}
    showScrollbar={false}
    itemRenderer={(hProps:RendererProps)=><HeaderComponent width={width} onResize={handleResize} frozenColumns={frozenColumns} {...hProps}></HeaderComponent>}
  ></Grid>
  <div style= {{display:"flex", flexDirection: "row", width:"100%", height:"100%"}} >

  <Grid
  width={100}
  height={height}
  columnCount={1}
  rowCount = {props.data.property.noRows}
  ref={rowHeaderRef}
  columnWidth = {(index:number)=>{
    if (index in columnWidthMap) return columnWidthMap[index];
    return 100;
  }}
  rowHeight = {(index)=>{
    return 20;
  }}
  showScrollbar={false}
  itemRenderer= {(hProps:RendererProps)=><RowHeaderComponent width={width} {...hProps}></RowHeaderComponent>}
  ></Grid>
  <Grid
  width={width-100}
  height={height}
  ref={bodyRef}
  columnCount={props.data.columns.length}
  rowCount = {props.data.property.noRows}
  columnWidth = {(index:number)=>{
    if (index in columnWidthMap) return columnWidthMap[index];
    return 100;
  }}
  rowHeight = {(index)=>{
    return 20;
  }}
  onScroll={(scrollPos:ScrollCoords)=>{
    headerRef.current?.scrollTo({scrollTop:0, scrollLeft:scrollPos.scrollLeft});
    rowHeaderRef.current?.scrollTo({scrollTop:scrollPos.scrollTop, scrollLeft:0});
  }}
  itemRenderer= {Cell}
  ></Grid></div></div>);
}