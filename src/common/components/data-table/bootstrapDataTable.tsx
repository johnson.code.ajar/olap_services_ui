import { DataTable } from "Model/data";
import React from "react";
import Table from "react-bootstrap/Table";
export const BootstrapDataTable = ({data}:{data:DataTable})=>{
  const TableRows = ()=>{
    if(!data) {
      return <></>;
    }
    if(data.property.noRows === 0) {
      return <></>;
    }
    const rows:React.ReactElement[] = [];
    for(let i=0; i< data?.property.noRows;i++) {
      rows.push(<tr key={`${data.property.name}_${i}`}>
      <td>{i}</td>
      {data.columns.map((col,j)=><td key={`${data.property.name}_${i}_${j}`}>{col.values[i]}</td>)}
      </tr>);
    }
    return rows;
  };

  return ( <Table striped bordered hover size="sm">
  <thead>
    <tr>
    <th>#</th>
    {data?.columns.map(col=><th key={col.name}>{col.name}</th>)}
    </tr>
  </thead>
  <tbody>
   {TableRows()}
  </tbody>
</Table>);
}