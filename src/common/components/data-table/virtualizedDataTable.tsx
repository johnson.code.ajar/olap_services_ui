import { DataTable } from "Model/data";
import React, { useRef, useState } from "react";
import { AutoSizer, CellMeasurer, CellMeasurerCache, Column, Table, TableCellProps, TableRowRenderer } from "react-virtualized";
import "react-virtualized/styles.css";
import { StyledTable } from "./dataTable.style";
export const VirtualizedDataTable = ({data}:{data:DataTable})=>{

  const [cache] = useState(
    new CellMeasurerCache({
      fixedWidth: true,
      minHeight: 35,
    })
  )

  const [selectedRowIndex, setSelectedRowIndex] = useState(0);
  const tableRef = useRef<Table>(null);
  const dataRow = (index:number)=>{
    return data.columns.map((col)=>{
      return col.values[index];
    });
  }

  const dataRow1 = ({index}:{index: number}) => {
    const row:{[key:string]:string}={};
    for(const column of data.columns) {
      row[column.name] = column.values[index]
    }
    return row;
  }

  const columnValue = (row:number, col:number) =>{
    return data.columns[col].values[row];
  }

    const _rowClassName = ({index}:{index:number})=>{
    if (index < 0) {
      return "headerRow";
    } else {
      return index % 2 === 0 ? "evenRow": "oddRow"; 
    }
  }

  const columnCellRenderer = (cellData: TableCellProps) => {
  const { dataKey, parent, columnIndex, rowIndex } = cellData
    const log = dataRow1({index:rowIndex})
    const content = log[dataKey]
    console.log(log);
    console.log(content)
    //const content = columnValue(rowIndex, columnIndex);
    return  <CellMeasurer
    cache={cache}
    columnIndex={columnIndex}
    key={dataKey}
    parent={parent}
    rowIndex={rowIndex}
  >
  <div>{content}</div>
  </CellMeasurer>
    
  }

  const rowRenderer:TableRowRenderer = ({index, key, style})=>{
    return (<div key={key} style={style}>
      {data.columns.map(column=><div key={`${index}_${key}_${column.name}`}>{column.values[index]}</div>)}
    </div>)
  }
  const MIN_WIDTH = 500;
  return(
  <AutoSizer>
  {({height, width})=><StyledTable
  height={height}
  width= {width < MIN_WIDTH ? MIN_WIDTH: width}
  rowHeight = {30}
  headerHeight = {30}
  columnCount = {10}
  gridStyle={{ outline: "none" }}
  rowCount = {data.property.noRows}
  rowGetter={dataRow1}
  headerClassName="headerColumn"
  //rowRenderer={rowRenderer}
  rowClassName={_rowClassName}
  ref={tableRef}
  >
    {data.columns.map(col=><Column flexShrink={0} maxWidth={200}  className="exampleColumn" cellRenderer={columnCellRenderer} width={200} key={col.name} label={col.name} dataKey={col.name}></Column>)}

  </StyledTable>}
  </AutoSizer>)

  // function generateData(){
  //   const data = [];
  //   for (let i = 0; i < 1000; i++) {
  //     data.push({ number: i, name: `number${i}` });
  //   }
  //   return data;
  // }

  // const [rowData, setRowData] = useState<{number:number;name:string}[]>([]);
  // useEffect(() => {
  //   setRowData(generateData());
  // }, []);

  // const columnCellRenderer = (cellData: TableCellProps) => {
  //   const { dataKey, parent, columnIndex, rowIndex } = cellData
  //   const log = rowData[rowIndex]
  //   const content = log[dataKey as keyof typeof log]

  //   return  <CellMeasurer
  //   cache={cache}
  //   columnIndex={columnIndex}
  //   key={dataKey}
  //   parent={parent}
  //   rowIndex={rowIndex}
  // >
  //  {/* {({registerChild})=>(<div ref={(element):void=>{
  //   if(element && registerChild) {
  //     registerChild(element)
  //   }
  //  }}>{content}</div>)} */}
  //  <div>{content}</div>
  // </CellMeasurer>
    
  // }



  // return(
  //   <AutoSizer>
  //     {({ height, width}) => (
  //       <StyledTable
  //         gridStyle={{ outline: "true" }}
  //         headerClassName="headerColumn"
  //         width={width}
  //         height={height}
  //         headerHeight={20}
  //         rowHeight={20}
  //         rowCount={rowData.length}
  //         rowClassName={_rowClassName}
  //         rowGetter={({index}:{index:number})=>rowData[index]}
  //         style={{border: "1px solid red"}}
  //       >
  //         <Column width={200} className="exampleColumn" label="Number" dataKey="number" cellRenderer={columnCellRenderer}/>
  //         <Column width={200} className="exampleColumn" label="Name" dataKey="name" cellRenderer={columnCellRenderer}/>
  //       </StyledTable>
  //     )}
  //   </AutoSizer>);
}