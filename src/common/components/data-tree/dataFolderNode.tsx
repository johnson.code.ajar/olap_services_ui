import { faAngleDown, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { DataFolder, DataProperty } from "Model/data";
import React, { useCallback, useState } from "react";
import { DataFileNode } from "./dataFileNode";
import { DataNodeName, DataUL } from "./dataTree.style";
interface Properties {
  folder: DataFolder;
  openNode: ()=>void;
  selectNode: (file:DataProperty)=>void;
}

export const DataFolderNode = ({folder, openNode, selectNode}:Properties)=>{
  const [isExpanded, setIsExpanded] = useState(false);
  const doubleClickAction = useCallback((e:React.MouseEvent<HTMLElement>)=>{
    setIsExpanded(!isExpanded);
  },[setIsExpanded,isExpanded]);

  if(folder.folders && folder.folders.length>0){
    return (<>{folder.folders.map(f=><DataFolderNode key={f.name} folder={f} openNode={openNode} selectNode={selectNode}></DataFolderNode>)}</>)
  } else if(folder.files && folder.files.length>0) {
    return(<DataUL>
      <span>
      <i style={{paddingRight:'5px'}} className="fas rotate" onDoubleClick={doubleClickAction}>
      {isExpanded?<FontAwesomeIcon icon={faAngleDown}></FontAwesomeIcon>:<FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon>}
      </i>
       <DataNodeName>{folder.name}</DataNodeName>
      </span>
      {isExpanded && folder.files.map(file=><DataFileNode key={file.name} file={file} openNode={openNode} selectNode={selectNode}></DataFileNode>)}
      </DataUL>);
  }
  return <></>;
}