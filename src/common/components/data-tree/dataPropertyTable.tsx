import { DataProperty } from "Model/data";
import React from "react";
export const DataPropertyTable  = ({property}:{property?:DataProperty})=>{
  if(!property){
    return<></>;
  }
  //TODO: limit the table size.
  return(
    <table className="table table-bordered">
      <thead>
        <tr>
        <th>Property</th>
        <th>Value</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Name</td>
          <td>{property.name}</td>
        </tr>
        <tr>
          <td>Delimiter</td>
          <td>{property.delimiter}</td>
        </tr>
        <tr>
          <td>HasHeader</td>
          <td><input type="checkbox"></input></td>
        </tr>
        <tr>
          <td>Encoded</td>
          <td><input type="checkbox"></input></td>
        </tr>
        <tr>
          <td>Date Format</td>
          <td><input type="text"></input></td>
        </tr>
        <tr>
          <td >Sample</td>
          <td><div style={{maxWidth:`${window.innerWidth-320}`,overflowX:"scroll"}}>
            <pre>
            {property.sampleData}
            </pre>
            </div></td>
        </tr>
        <tr>
          <td>Type</td>
          <td>{property.extension}</td>
        </tr>
      </tbody>
    </table>)
}