import { DataLI, DataNodeName } from "Components/data-tree/dataTree.style";
import { DataProperty } from "Model/data";
import React, { useCallback } from "react";
interface Properties {
  file: DataProperty;
  openNode: ()=>void;
  selectNode: (file:DataProperty)=>void;
}
export const DataFileNode = ({file, openNode, selectNode}:Properties)=>{
  const selectFileNode = useCallback((e:React.MouseEvent, node:DataProperty)=>{
    selectNode(node);
  },[selectNode]);

  return(<DataLI>
    <span>
    <DataNodeName onDoubleClick={openNode} onClick={(e)=>selectFileNode(e,file)}>{file.name}</DataNodeName>
    </span>
  </DataLI>)
}