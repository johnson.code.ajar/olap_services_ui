import { DataFolderNode } from "Components/data-tree/dataFolderNode";
import { DataTreeContainer } from "Components/data-tree/dataTree.style";
import { DataFolder, DataProperty } from "Model/data";
import React from "react";
interface Properties {
  folders : DataFolder;
  openNode: ()=>void;
  selectNode: (file:DataProperty)=>void;
}
export const DataTree = ({folders, openNode, selectNode}:Properties)=>{
    return(<DataTreeContainer>
    <DataFolderNode folder={folders} openNode={openNode} selectNode={selectNode}></DataFolderNode>
  </DataTreeContainer>);
}