import styled from "styled-components";
export const DataTreeContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const DataUL = styled.ul`
  display: flex;
  flex-direction: column;
  padding-inline-start: 5px;
  height: 100%;
  margin-bottom:0px;
  font-size:14px;
  list-style-type:none;
  // on hover span color change has to be define in parent and child
  :hover {
    &> span {
      color: #0d6efd;
    }
  }
  &> span {
    :hover {
      color: #0d6efd;
      font-size: 15px;
    }
  }
`
export const DataLI = styled.li`
  display: flex;
  padding-inline-start: 20px;
  :hover {
    > span {
      color: #0d6efd;
      font-size: 15px;
    }
  }
`

export const DataNodeName = styled.label`
  padding-left:2px;
`