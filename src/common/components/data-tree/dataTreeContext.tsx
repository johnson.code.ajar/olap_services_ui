import { DataFolder, DataProperty } from "Model/data";
import { dummyFolderData } from "Model/sampledata";
import React, { Dispatch, createContext, useReducer } from "react";

export interface DataTreeState {
  folders: DataFolder;
}

const rootFolder: DataTreeState = {
  // folders:{
  //   name: "root",
  //   folders:[],
  //   files:[]
  // }
  folders: dummyFolderData
};

export enum DataTreeActionType {
  SET_FOLDER = "SET_FOLDER",
  ADD_FOLDER = "ADD_FOLDER",
  DELETE_FOLDER = "DELETE_FOLDER",
  RENAME_FOLDER = "RENAME_FOLDER",
  ADD_FILE = "ADD_FILE",
  DELETE_FILE = "DELETE_FILE",
  RENAME_FILE = "RENAME_FILE"
}

export type DataTreeActions = | {type: "SET_FOLDER",
  payload: {folder:DataFolder}
} | {
  type: "ADD_FOLDER",
  payload: {folder:DataFolder}
} | {
  type: "DELETE_FOLDER",
  payload: {folder: DataFolder}
} | {
  type: "RENAME_FOLDER",
  payload: {folder: DataFolder, name:string}
} | {
  type: "ADD_FILE",
  payload: {file: DataProperty}
} | {
  type: "DELETE_FILE",
  payload: {file: DataProperty}
} | {
  type: "RENAME_FILE",
  payload: {file: DataProperty}
}


const dataTreeReducers = (state: DataTreeState = rootFolder, action: DataTreeActions) => {
  let partialState: DataTreeState | undefined;
  switch(action.type) {
    case "SET_FOLDER":
      partialState = {
        folders:action.payload.folder
      }
    break;
    case "ADD_FOLDER":
    break;
    case "DELETE_FOLDER":
    break;
    case "RENAME_FOLDER":
    break;
    case "ADD_FILE":
    break;
    case "DELETE_FILE":
    break;
    case "RENAME_FILE":
    break;
  }
  return partialState ? {...state, ...partialState}: state;
}

interface DataTreeContextType {
  state: DataTreeState;
  dataTreeDispatch: Dispatch<DataTreeActions>;
}

const DataTreeContext = createContext<DataTreeContextType>({
  state: rootFolder,
  dataTreeDispatch:()=>null 
});

const DataTreeProvider:React.FC<React.PropsWithChildren> = ({children})=>{
  const [state, dataTreeDispatch] = useReducer(dataTreeReducers, rootFolder);
  return (<DataTreeContext.Provider value={{state, dataTreeDispatch}}>{children}</DataTreeContext.Provider>);
}

export { DataTreeContext, DataTreeProvider };

