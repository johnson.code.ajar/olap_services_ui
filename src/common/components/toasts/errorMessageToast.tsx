import React, { useCallback, useState } from 'react';

export interface Message {
  error?: string;
  warning?: string;
  type?: string;
}

interface Props {
  messages: Message[];
}

const ErrorMessageToast = ({messages}:Props) => {
  const ErrorToast = ({message}:{message:Message})=>{
    const [show, setShow] = useState(true);
    const onClose = useCallback((e:React.MouseEvent<HTMLButtonElement>)=>{
      setShow(false);
    },[setShow]);
    if(!show){
      return <></>;
    }
    return(<div   style={{display:"flex", flexDirection:"column"}} aria-live="assertive" className="toast" role="alert">
    <div className="toast-header"><strong className="me-auto">Message</strong>
    <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close" onClick={onClose}></button>
    </div>
    <div className="toast-body">
      {message.error}
    </div>
  </div>);
  }
  
  if(messages.length===0 ) {
    return <></>
  }
  return (<div className="toast-container position-absolute top-0 end-0">
  {messages.map(m=><ErrorToast key={m.error} message={m}></ErrorToast>
    )}
  </div>);
}

export default ErrorMessageToast;