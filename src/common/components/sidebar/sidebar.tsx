import * as React from 'react';
import { SideBarContainer } from '../../../app/appStyle';
type SideBarProps = {
  isOpen: boolean;
};

const SideBar = (props: React.PropsWithChildren<SideBarProps>)=>{
  return (props.isOpen?<SideBarContainer id="sidebar-container" hidden={!props.isOpen}>
    {props.children}
    </SideBarContainer>:<></>)
}

export default SideBar;