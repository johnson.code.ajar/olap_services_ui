
import React from 'react';
import { HeaderContainer } from "../../../app/appStyle";

const ToolbarMenus = () => {

  return(
    <>
    <div role="toolbar" className="btn-toolbar btn-group-sm me-2"  style={{ alignItems:'center', display:'flex'}}>
      <button type="button" className="btn">File</button>
      <button type="button" className="btn">Add</button>
      <button type="button" className="btn ">Delete</button>
      <button type="button" className="btn" >Run</button>
      <button type="button" className="btn" >Import</button>
    </div>
    </>);
}

const NavigationBar = ({title}:{title:string;}) =>{
  
  return(
  <HeaderContainer>
    <ToolbarMenus/>
  </HeaderContainer>);
}

export default NavigationBar;