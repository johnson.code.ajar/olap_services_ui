export interface EventMessage {
  topic:string;
  message: string;
}