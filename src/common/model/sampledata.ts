import { DataFolder } from "Model/data";
export const dummyFolderData:DataFolder = {
  "name": "data",
  "folders": [
      {
          "name": "medical",
          "folders": [],
          "files": [
              {
                  "name": "breast-cancer",
                  "id": "breast-cancer",
                  "location":"breast-cancer.csv",
                  "delimiter": null,
                  "extension": "csv",
                  "noIp": 0,
                  "noOp": 0,
                  "noRows": 0,
                  "encoded": false,
                  "hasHeader": false,
                  "sampleData": "id,diagnosis,radius_mean,texture_mean,perimeter_mean,area_mean,smoothness_mean,compactness_mean,concavity_mean,concave points_mean,symmetry_mean,fractal_dimension_mean,radius_se,texture_se,perimeter_se,area_se,smoothness_se,compactness_se,concavity_se,concave points_se,symmetry_se,fractal_dimension_se,radius_worst,texture_worst,perimeter_worst,area_worst,smoothness_worst,compactness_worst,concavity_worst,concave points_worst,symmetry_worst,fractal_dimension_worst842302,M,17.99,10.38,122.8,1001,0.1184,0.2776,0.3001,0.1471,0.2419,0.07871,1.095,0.9053,8.589,153.4,0.006399,0.04904,0.05373,0.01587,0.03003,0.006193,25.38,17.33,184.6,2019,0.1622,0.6656,0.7119,0.2654,0.4601,0.1189842517,M,20.57,17.77,132.9,1326,0.08474,0.07864,0.0869,0.07017,0.1812,0.05667,0.5435,0.7339,3.398,74.08,0.005225,0.01308,0.0186,0.0134,0.01389,0.003532,24.99,23.41,158.8,1956,0.1238,0.1866,0.2416,0.186,0.275,0.0890284300903,M,19.69,21.25,130,1203,0.1096,0.1599,0.1974,0.1279,0.2069,0.05999,0.7456,0.7869,4.585,94.03,0.00615,0.04006,0.03832,0.02058,0.0225,0.004571,23.57,25.53,152.5,1709,0.1444,0.4245,0.4504,0.243,0.3613,0.08758",
                  "dateFormat": "yyyy-mm-dd"
              },
              {
                  "name": "diabetes",
                  "id": "diabetes",
                  "location": "diabetes.csv",
                  "delimiter": null,
                  "extension": "csv",
                  "noIp": 0,
                  "noOp": 0,
                  "noRows": 0,
                  "encoded": false,
                  "hasHeader": false,
                  "sampleData": "Pregnancies,Glucose,BloodPressure,SkinThickness,Insulin,BMI,DiabetesPedigreeFunction,Age,Outcome9,120,72,22,56,20.8,0.733,48,01,71,62,0,0,21.8,0.416,26,08,74,70,40,49,35.3,0.705,39,0",
                  "dateFormat": "yyyy-mm-dd"
              }
          ]
      },
      {
          "name": "samples",
          "folders": [],
          "files": [
              {
                  "name": "IRIS",
                  "id": "iris",
                  "location":"iris.csv",
                  "delimiter": null,
                  "extension": "csv",
                  "noIp": 0,
                  "noOp": 0,
                  "noRows": 0,
                  "encoded": false,
                  "hasHeader": false,
                  "sampleData": "sepal_length,sepal_width,petal_length,petal_width,species5.1,3.5,1.4,0.2,Iris-setosa4.9,3,1.4,0.2,Iris-setosa4.7,3.2,1.3,0.2,Iris-setosa",
                  "dateFormat": "yyyy-mm-dd"
              }
          ]
      }
  ],
  "files": []
};