export interface DataProperty {
  name:string;
  id: string;
  location: string;
  delimiter: string|null;
  extension: string;
  noIp: number;
  noOp: number;
  noRows: number;
  encoded: boolean;
  hasHeader: boolean;
  sampleData: string;
  dateFormat: "yyyy-mm-dd";
}
export interface DataFolder {
  name:string;
  folders:DataFolder[];
  files: DataProperty[]
}

export interface DataTable {
  property: DataProperty;
  columns:DataColumn[];
}

export interface DataRow {
  inputs:string[];
  outputs:string[];
}

export interface DataColumn {
  name:string;
  dataType: string;
  type: string;
  values:string[];
}