import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { DataFolder, DataProperty } from "Model/data";
import { DataTable } from "ModellerAppModels/dataset";
class DataService {

  private dataServiceUrl:AxiosInstance  = axios.create({
    baseURL: `${process.env.MODELLER_API_BASE_URL}/`
  });

  public getAllData = async ()=>{
    const data:DataFolder = await this.dataServiceUrl.get("/modeller/data").then(response=>response.data);
    return data;
  }

  public getData = async (dataProperty:DataProperty)=>{
    const config:AxiosRequestConfig<DataProperty> = {
      url: `${process.env.MODELLER_API_BASE_URL}/modeller/data/${dataProperty.name}`,
      method: 'POST',
      maxBodyLength: Infinity,
      headers: {
        'Content-Type': 'application/json'
      },
      data:dataProperty
    }
    const data:DataTable = await axios.request(config).then(response=> response.data);
    //const data:DataTable = await this.dataServiceUrl.get(`/modeller/data/${dataProperty.name}`, dataProperty).then(response=>response.data);
   
    return data;
  }

}

const dataService:DataService = new DataService();

export default dataService;