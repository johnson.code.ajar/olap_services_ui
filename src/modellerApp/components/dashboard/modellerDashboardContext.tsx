import { DataProperty } from "Model/data";
import React, { Dispatch, createContext, useReducer } from "react";
interface ModellerDashboardState {
  tabs: {[key:string]:DataProperty};
  selectedTab?: DataProperty;
}

const initialDashboardState = {
  tabs:{},
}

type ModellerDashboardActions = | {
  type: "ADD_TAB",
  payload: {tab:DataProperty}
} | 
{
  type: "REMOVE_TAB",
  payload: {tab:DataProperty}
}|{
  type: "SET_TAB_SELECTED",
  payload: {tab:DataProperty}
}


const modellerDasboardReducers = (state: ModellerDashboardState, action: ModellerDashboardActions) => {
  let partialState: ModellerDashboardState | undefined;
  switch(action.type) {
    case "ADD_TAB":
      const tabs= state.tabs;
      tabs[action.payload.tab.id]=action.payload.tab;
      partialState ={
        tabs
      }
    break;
    case "REMOVE_TAB":
      const openedTabs = state.tabs;
      delete openedTabs[action.payload.tab.id];
      partialState = {
        tabs: openedTabs
      }
    break;
    case "SET_TAB_SELECTED": // TODO: Move this app store so it can be shared by different components in the same level.
      partialState = {
        ...state,
        selectedTab: action.payload.tab
      }
    break;
  }
  return partialState ? {...state, ...partialState}:state;
}

type ModellerDashboardContextType = {
  dashboardState: ModellerDashboardState;
  dashboardDispatch: Dispatch<ModellerDashboardActions>;
}


const ModellerDashboardContext = createContext<ModellerDashboardContextType>({
  dashboardState: initialDashboardState,
  dashboardDispatch: ()=>null
});

const ModellerDashboardProvider:React.FC<React.PropsWithChildren> = ({children})=>{
  const [dashboardState, dashboardDispatch] = useReducer(modellerDasboardReducers, initialDashboardState);
  return (<ModellerDashboardContext.Provider value={{dashboardState, dashboardDispatch}}>
    {children}
  </ModellerDashboardContext.Provider>);
}

export { ModellerDashboardContext, ModellerDashboardProvider };
