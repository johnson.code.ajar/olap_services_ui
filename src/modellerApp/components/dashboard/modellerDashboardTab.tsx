import { DataProperty, DataTable } from "Model/data";
import { ModellerDashboardContext } from "ModellerAppComponents/dashboard/modellerDashboardContext";
import dataService from "ModellerAppService/dataService";
import { ModellerDashboardTabContainer } from "./modellerDashboard.style";
// import "bootstrap/dist/css/bootstrap.min.css";
import { CanvasDataTable } from "Components/data-table/canvasDataTable";
import React, { SyntheticEvent, useCallback, useContext, useEffect, useState } from "react";
export const ModellerDashboardTab = ({tab}:{tab:DataProperty})=>{
  const {dashboardState, dashboardDispatch} = useContext(ModellerDashboardContext);
  const [data, setData] = useState<DataTable>();

  useEffect(()=>{
    const getData = async ()=> await dataService.getData(tab);
    getData().then(data=>{
      setData(data);
    });
  },[tab, setData])

  const onTabSelect:React.MouseEventHandler<HTMLDivElement> = useCallback((e:SyntheticEvent)=>{
    dashboardDispatch({
      type: "SET_TAB_SELECTED",
      payload: {tab: tab}
    });
  },[]);
  if(!data){
    return <></>
  }

  return <ModellerDashboardTabContainer id="modeller-dashboard-tab" onClick={onTabSelect}>
    {/* {data&&<BootstrapDataTable data={data}></BootstrapDataTable>} */}
    {/* {data && <VirtualizedDataTable  data={data}></VirtualizedDataTable>} */}
    <CanvasDataTable data={data}></CanvasDataTable>
  </ModellerDashboardTabContainer>
}