import styled from "styled-components";

export const ModellerDashboardTabContainer = styled.div`
  display: inline-block;
  height: 100%;
  width: 100%;
  // overflow:auto; add a flag to include overflow.
`