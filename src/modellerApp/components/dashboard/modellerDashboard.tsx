import { faClose } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { DataProperty } from "Model/data";
import { ModellerDashboardContext } from "ModellerAppComponents/dashboard/modellerDashboardContext";
import React, { useCallback, useContext } from "react";
import { Tab } from "react-bootstrap";
import Tabs from "react-bootstrap/Tabs";
import { ModellerDashboardTab } from "./modellerDashboardTab";
export const ModellerDashboard = ()=>{
  const {dashboardState, dashboardDispatch } = useContext(ModellerDashboardContext);
  const DashboardTabTitle = ({tab}:{tab:DataProperty})=>{
    const closeTab = useCallback(()=>{
      dashboardDispatch({
        type:"REMOVE_TAB",
        payload:{tab:tab}
      });
    },[tab]);
    return<span>
      {tab.name}
      <a style={{paddingLeft:'10px'}} href='#' onClick={closeTab}><FontAwesomeIcon icon={faClose}></FontAwesomeIcon></a>
    </span>
  };
  return(
    <Tabs>
      {Object.values(dashboardState.tabs).map(tab=>
      <Tab key={tab.id} eventKey={tab.id} title={<DashboardTabTitle tab={tab}></DashboardTabTitle>}>
        <ModellerDashboardTab tab={tab}></ModellerDashboardTab>
      </Tab>)}
    </Tabs>
  );
};