import { DataTreeContext, DataTreeProvider } from "Components/data-tree/dataTreeContext";
import { ModellerNavItem, ModellerSideBarContent, ModellerSideBarTabsContainer } from "ModellerAppComponents/tabs/modellerSideTabs.style";
import React, { useContext } from "react";
import { Nav, Tab } from "react-bootstrap";
import { ModellerDataTree } from "./modellerDataTree";

export const ModellerSideBarTabs = ()=>{
  const {state, dataTreeDispatch} = useContext(DataTreeContext);


  return (<Tab.Container>
      <ModellerSideBarContent>
        <Tab.Pane eventKey="Data">
          <DataTreeProvider>
            <ModellerDataTree></ModellerDataTree>
          </DataTreeProvider>
        </Tab.Pane>
        <Tab.Pane eventKey="Model">
          <div>Model</div>
        </Tab.Pane>
        <Tab.Pane eventKey="Experiments">
          <div>Experiments</div>
        </Tab.Pane>
      </ModellerSideBarContent>
      <ModellerSideBarTabsContainer variant="pills" defaultActiveKey="Data">
        <ModellerNavItem><Nav.Link eventKey="Data">Data</Nav.Link></ModellerNavItem>
        <ModellerNavItem><Nav.Link eventKey="Model">Models</Nav.Link></ModellerNavItem>
        <ModellerNavItem><Nav.Link eventKey="Experiments">Experiments</Nav.Link></ModellerNavItem>
      </ModellerSideBarTabsContainer>
  </Tab.Container>);
}