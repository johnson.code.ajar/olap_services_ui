import { DataTree } from "Components/data-tree/dataTree";
import { DataTreeContext } from "Components/data-tree/dataTreeContext";
import { DataFolder, DataProperty } from "Model/data";
import { ModellerDashboardContext } from "ModellerAppComponents/dashboard/modellerDashboardContext";
import dataService from "ModellerAppService/dataService";
import React, { useCallback, useContext, useEffect } from "react";
export const ModellerDataTree = ()=>{
  const {state, dataTreeDispatch} = useContext(DataTreeContext);
  const {dashboardState, dashboardDispatch} = useContext(ModellerDashboardContext);
  useEffect(()=>{
    const getFolders = async ()=> await dataService.getAllData();
    getFolders().then((data)=>{
      console.log(data);
      dataTreeDispatch({
      type: "SET_FOLDER",
      payload: {folder:data as DataFolder}
    })});
  },[])

  const openNode = useCallback(()=>{
    if(!dashboardState.selectedTab) {
      return;
    }
    dashboardDispatch({
      type: "ADD_TAB",
      payload: {tab:dashboardState.selectedTab}
    })
  },[dashboardDispatch, dashboardState.selectedTab])

  const selectNode = useCallback((node: DataProperty)=>{
    dashboardDispatch({
      type: "SET_TAB_SELECTED",
      payload: {tab:node}
    })
  },[dashboardDispatch]);

  return(<div style={{  display:"inline-block", height:"100%",width:"100%", flexDirection: "column",minWidth:"200px",
    overflowX: "scroll"}}>
    <DataTree folders={state.folders} openNode={openNode} selectNode={selectNode}></DataTree>
    </div>
 );
}