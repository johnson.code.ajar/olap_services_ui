import { Nav, NavItem, Tab } from "react-bootstrap";
import styled from "styled-components";

export const ModellerSideBarContent= styled(Tab.Content)`
display: inline-block;
block-size:90%;  
width: 100%;
.tab-pane active show {
  width: 100%;
  block-size: 90%;
  height:100%;
}
`

export const ModellerSideBarTabsContainer = styled(Nav)`
  display: inline-block;
  width: 100%;
  block-size: 10%;
  flex-direction: row;
  border: 1px solid green;
  align-items: top;
`

export const ModellerNavItem = styled(NavItem)`
  display: inline-block;
  width: 100%;
  text-align: center;
  padding: 0px;
  background: rgb(232, 245, 253);
  .nav-link {
    width: 100%;
    //height: 100%;
    margin: 0 auto;
  }
`