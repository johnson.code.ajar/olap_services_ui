import { ModellerDashboardContext } from "ModellerAppComponents/dashboard/modellerDashboardContext";
import { ModellerDataPropertyTab } from "ModellerAppComponents/footer/modellerDataPropertyTab";
import React, { useContext } from "react";
import { Tab } from "react-bootstrap";
import Tabs from "react-bootstrap/Tabs";
export const ModellerFooterTabs = ()=>{
  const {dashboardState, dashboardDispatch} = useContext(ModellerDashboardContext);
  if(!dashboardState.selectedTab) {
    return <></>;
  }
  return(
    <Tabs>
    <Tab eventKey="Property" title={dashboardState.selectedTab.name}>
      <ModellerDataPropertyTab property={dashboardState.selectedTab}></ModellerDataPropertyTab>
    </Tab>
  </Tabs>);
}