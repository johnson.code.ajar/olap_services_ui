import { DataPropertyTable } from "Components/data-tree/dataPropertyTable";
import { DataProperty } from "Model/data";
import React from "react";
export const ModellerDataPropertyTab = ({property}:{property?:DataProperty})=>{
  return (
    <div style={{display:'block', minHeight:"150px"}}>
      <DataPropertyTable property={property}></DataPropertyTable>
    </div>
  )
}