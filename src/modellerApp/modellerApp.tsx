import NavigationBar from "Components/navigationBar/NavigationBar";
import SideBar from "Components/sidebar/sidebar";
import { ModellerDashboard } from "ModellerAppComponents/dashboard/modellerDashboard";
import { ModellerDashboardProvider } from "ModellerAppComponents/dashboard/modellerDashboardContext";
import { ModellerFooterTabs } from "ModellerAppComponents/footer/modellerFooterTabs";
import { ModellerSideBarTabs } from "ModellerAppComponents/tabs/modellerSideTabs";
import React from "react";
import { BodyContainer, DashboardTabsContainer, FooterContainer, WorkspaceContainer } from "../app/appStyle";
import '../scss/styles.scss';
import { ModellerAppContainer } from "./modellerApp.style";
export const ModellerApp = ({isSideBarOpen}:{isSideBarOpen:boolean})=>{
  return(<ModellerAppContainer>
    <NavigationBar title="Modelling Matters"></NavigationBar>
    <BodyContainer>
      <ModellerDashboardProvider>
        <SideBar isOpen={isSideBarOpen}>
          <ModellerSideBarTabs></ModellerSideBarTabs>
        </SideBar>
        <WorkspaceContainer isOpen={isSideBarOpen} id="modeller-workspace-container">
            <DashboardTabsContainer id="modeller-dashboard-tabs-container">
              <ModellerDashboard></ModellerDashboard>
            </DashboardTabsContainer>
            <FooterContainer>
              <ModellerFooterTabs></ModellerFooterTabs>
            </FooterContainer>
        </WorkspaceContainer>
        </ModellerDashboardProvider>
    </BodyContainer>
  </ModellerAppContainer>);
}