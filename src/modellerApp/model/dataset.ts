import { DataProperty } from "Model/data";

export interface DataColumn {
  name: string;
  dataType: string;
  type: string;
  values:string[];
}

export interface DataTable {
  property: DataProperty;
  columns: DataColumn[];
}