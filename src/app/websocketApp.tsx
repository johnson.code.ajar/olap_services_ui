import React, { useCallback, useEffect, useRef, useState } from "react";
const WebsocketApp = () => {
  //TODO: Check if we need all this wsRef to handle websocket....
  const wsRef = useRef<WebSocket|null>(null)
  const [message, setMessage] = useState('');
  const [receivedMessage, setReceivedMessage] = useState('')
  const [isOpen, setIsOpen] = useState(false);
  const [messages, setMessages] = useState<string[]>([]);
  const [waitingToReconnect, setWaitingToReconnect] = useState<boolean|null>(null)
  // const [ws, _] = useState(new WebSocket('ws://localhost:8000/ws'));
  const [ws, _] = useState(new WebSocket("ws://localhost:5101/messages"));
  useEffect(()=>{
    if(waitingToReconnect){
      return;
    }
    if(!wsRef.current){
      wsRef.current = ws
      
      ws.onerror = (e) => console.error(e);
      ws.onopen = (e:Event) => {
        setIsOpen(true)
      }
      ws.onclose = () => {
        if (wsRef.current){
          console.log("Websocket closed by server");
        } else {
          console.log("Websocket closed by app component unmount")
          return
        }
        if(waitingToReconnect){
          return;
        }
        
        setIsOpen(false)
        console.log("Websocket closed....")
        setWaitingToReconnect(true);
        setTimeout(()=>setWaitingToReconnect(null), 5000);
      }
      ws.onmessage = (msg:MessageEvent) => {
        console.log(msg.data);
        console.log(ws.url)
        setReceivedMessage(msg.data);
        setMessage("");
      }
    }
    return () => {
      console.log("Cleaning websocket connection....")
      wsRef.current = null
      ws.close();
    }
  },[setReceivedMessage, waitingToReconnect])
  
  useEffect(()=>{
    if(receivedMessage){
      setMessages(existingMsgs => [...existingMsgs, receivedMessage])
    }
  },[receivedMessage, setMessages])

  const sendMessage = useCallback(async (event:React.FormEvent<HTMLFormElement>)=>{
    event.preventDefault()
    console.log(`Sending websocket message....${message}`)
    // await axios.get(`http://localhost:8000/push/${message}`);
    const data = JSON.stringify({'name': message})
    console.log(data);
    ws.send(data)
  },[message])

  const inputMessage = useCallback((event:React.ChangeEvent<HTMLInputElement>)=>{
    const msg = event.target.value
    setMessage(msg)

  },[setMessage]);

  return (<div>
    <h1>Websocket Application {isOpen? "Connected": "Disconnected"}</h1>
    <form action = "" onSubmit={sendMessage}>
      <input type="text" value={message} id="messageText" onChange={inputMessage}/>
      <button>Send</button>
    </form>
    {messages.map((m)=><ul key={m}>{m}</ul>)}
  </div>)
}

export default WebsocketApp