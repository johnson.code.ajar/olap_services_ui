import styled from "styled-components";

export const AppContainer = styled.div`
  display:flex;
  flex-direction:column;
  height:100%;
  width:100%;
  background: rgb(232, 245, 253);
`

export const HeaderContainer = styled.div`
  order:1;
  display: flex;
  flex-direction: column;
  width: 100%;
  justify-content: center;
  border-bottom:1px solid black;
`

export const BodyContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction:row;
  height:100%;
  width:100%;
  flex:100%;
  order:2;
`



export const DashboardTabsContainer = styled.div`
  display:inline-block;
  height:70%;
  width:100%;
  vertical-align: top;
  margin-bottom:25px;
`

export const FooterContainer = styled.div`
  display: inline-block;
  flex-direction:column;
  order:2;
  border-top:1px solid black;
  height:30%;
  width:100%;
  justify-content:bottom;
  overflow: auto;
  vertical-align:bottom;
  
`

export const SideBarContainer = styled.div`
  diplay:block;
  height:100%;
  width:20%;
  border-right:1px solid black;
  block-size:{props=>props.hidden?0:15}%;
`

export const WorkspaceContainer = styled.div<{isOpen:boolean}>`
  display:block;
  width:${(props)=>props.isOpen?85:100}%;
  height:100%;
`
//TODO: used in olapApp remove it. 
export const DashboardContainer = styled.div`
  display:flex;
  flex-direction: column;
  order:1;
`

export const ToolbarContainer = styled.div`
  display: flex;
  flex-direction:row;
  flex: 0 0 25px;
  border: 1px solid green;
`

export const MainMenuContainer = styled.div`
  display: flex;
  flex-direction: row;
  height:30px;
  justify-content: space-between;
  background: #0d6efd;
  align-items: center;
`;

export const UserMenuContainer = styled.div`
  padding-right:20px;
  .btn {
    background: transparent;
    border:none;
  }
`
