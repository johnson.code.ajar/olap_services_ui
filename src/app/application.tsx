import { faEllipsisVertical } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { GraphOutput } from "ExpressionAppModels/expression";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { Dropdown } from 'react-bootstrap';
import { useDispatch } from "react-redux";
import { EventMessage } from "../common/model/eventMessage";
import ExpressionApp from "../expressionApp/expressionApp";
import { expressionAppActionCreators } from "../expressionApp/store/actions/index";
import { ModellerApp } from "../modellerApp/modellerApp";
import OlapApp from "../olapApp/olapApp";
import { AppContainer, MainMenuContainer, UserMenuContainer } from './appStyle';
import WebsocketApp from "./websocketApp";

interface AppDetail {
  name:string;
  id:number;
}

const appDetails:AppDetail[] = [
  {name: 'Planning', id:1},
  {name: 'Modelling', id:2},
  {name: 'Expression', id:3},
  {name: 'Optimisation', id:4},
  {name: 'Websocket',id:5}
];

const getApplication = (app:AppDetail, isSideBarOpen: ()=>boolean)=>{
  if(app.id === 1) {
    return <OlapApp isSideBarOpen={isSideBarOpen}/>
  } else if (app.id ===2 ) {
    return <ModellerApp isSideBarOpen={isSideBarOpen()}></ModellerApp>
  } else if(app.id == 3){
    return <ExpressionApp isSideBarOpen={isSideBarOpen}/>
  } else if (app.id === 5) {
    return <WebsocketApp/>
  }
  return <div>Empty Application</div>
}

const Application = () => {
  const [ws, _] = useState(new WebSocket(`${process.env.MESSAGE_API_BASE_URL}`));
  const wsRef = useRef<WebSocket|null>(null)
  const [receivedMessage, setReceivedMessage] = useState<EventMessage>()
  const [isSocketOpen, setIsSocketOpen] = useState(false);
  const [messages, setMessages] = useState<string[]>([]);
  const [waitingToReconnect, setWaitingToReconnect] = useState<boolean|null>(null)
  const appDispatch = useDispatch();
  const [activeApp, setActiveApp] = useState<AppDetail>(appDetails[2]);
  const [isSideBarOpen, setIsSideBarOpen] = useState(true);

  const toggleSidebar = useCallback(()=>{
    setIsSideBarOpen(!isSideBarOpen);
  },[isSideBarOpen, setIsSideBarOpen]);

  useEffect(()=>{
    if(waitingToReconnect){
      return;
    }
    if(!wsRef.current){
      wsRef.current = ws
      
      ws.onerror = (e) => console.error(e);
      ws.onopen = (e:Event) => {
        setIsSocketOpen(true)
      }
      ws.onclose = () => {
        if (wsRef.current){
          console.log("Websocket closed by server");
        } else {
          console.log("Websocket closed by app component unmount")
          return
        }
        if(waitingToReconnect){
          return;
        }
        
        setIsSocketOpen(false)
        console.log("Websocket closed....")
        setWaitingToReconnect(true);
        setTimeout(()=>setWaitingToReconnect(null), 5000);
      }
      ws.onmessage = (msg:MessageEvent) => {
        console.log(msg.data as EventMessage);
        console.log(ws.url)
        const data:EventMessage = JSON.parse(msg.data);
        setReceivedMessage(data);
      }
    }
    return () => {
      console.log("Cleaning websocket connection....")
      wsRef.current = null
      ws.close();
    }
  },[setReceivedMessage, waitingToReconnect])
  
  useEffect(()=>{
    if(receivedMessage){
      if(receivedMessage.topic === 'expressionOutput') {
        const output:GraphOutput = JSON.parse(receivedMessage.message);
        console.log(output);
        appDispatch(expressionAppActionCreators.expressions.setExpressionOutput(output.id, output));
        return;
      }
      setMessages([...messages, receivedMessage.message]) 
    }
  },[receivedMessage, setMessages])

  const toastMessages = ()=> <div className="toast-container position-absolute top-0 end-0">
    {messages.map((message, i) =>
      <div key={i} style={{display:"flex", flexDirection:"column"}} className="toast" role="alert">
        <div className="toast-header"><strong className="me-auto">Message</strong>
        <button type="button" className="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div className="toast-body">
          {message}
        </div>
      </div>)}
    </div>;
  
  const ToogleButton = () => {
    return(
    <button className="btn" type="button" data-toggle="collapse" 
          style={{ background:'transparent', border: 'none'}} onClick={toggleSidebar}>
      <FontAwesomeIcon icon={faEllipsisVertical}></FontAwesomeIcon>
  </button>)
  }
  
  const LoginMenu = ()=>{
    return(<UserMenuContainer>
    <button className="btn">Login</button>
    </UserMenuContainer>)
  }
  
  const onAppSelection = useCallback((appNo:number) => {
    console.log("Selecting application... "+appNo);
    setActiveApp(appDetails[appNo-1]);
  },[setActiveApp]);

  const AppSelectionMenu = () => {
    return(<Dropdown id="application-selection-dropdown">
      <Dropdown.Toggle style={{background:"transparent", border:"none"}}>
        {activeApp.name}
      </Dropdown.Toggle>
      <Dropdown.Menu>
        {appDetails.map((app,i)=><Dropdown.Item key={`${app.name}_${app.id}`} href="#" onClick={()=>onAppSelection(app.id)}>{app.name}</Dropdown.Item>)}
      </Dropdown.Menu>
    </Dropdown>);
  }

  const MainMenu = ()=>{
    return (<MainMenuContainer>
      <ToogleButton/>
      <AppSelectionMenu/>
      <LoginMenu/>
    </MainMenuContainer>)
  }

  const getIsSideBarOpen = ()=>isSideBarOpen;

  return( 
  <AppContainer>
    <MainMenu/>
   {getApplication(activeApp, getIsSideBarOpen)}
   {toastMessages()}
  </AppContainer>);
};

export default Application;