
import NavigationBar from "Components/navigationBar/NavigationBar";
import SideBar from 'Components/sidebar/sidebar';
import ErrorMessageToast from "Components/toasts/errorMessageToast";
import { ExpressionFooterTabs } from "ExpressionAppComponents/footer/expressionFooterTabs";
import ExpressionSideTabs from "ExpressionAppComponents/tabs/expressionSideTabs";
import { ExpressionGroup, ExpressionMessage } from "ExpressionAppModels/expression";
import expressionService from "ExpressionAppServices/expressionService";
import React, { useEffect } from 'react';
import { connect, useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { BodyContainer, DashboardTabsContainer, FooterContainer, WorkspaceContainer } from "../app/appStyle";
import '../scss/styles.scss';
import ExpressionDashboard from "./components/dashboard/expressionDashboard";
import { ExpressionDashboardProvider } from "./components/dashboard/expressionDashboardContext";
import { ExpressionAppContainer } from "./expressionApp.style";
import { ExpressionAppState } from "./store";
import { expressionActionCreators } from "./store/actions/expressionAction";

interface Props {
  expressionGroups: ExpressionGroup[];
  messages: ExpressionMessage[];
  isSideBarOpen: ()=>boolean;
  setExpressionGroups: (expressionGroups: ExpressionGroup[])=>any;
}

const ExpressionApp = ({expressionGroups, messages, isSideBarOpen, setExpressionGroups}:Props):JSX.Element => {
  const dispatch:Dispatch = useDispatch();
 
  useEffect(() => {
    const getExpressionGroups = async () => await expressionService.getExpressionGroups(dispatch);
    getExpressionGroups().then((expressionGroups)=>{
      setExpressionGroups(expressionGroups);
    });
    
  },[])


  // Check react-observer hook to alter the height and width of the Sidebar
  // Dashboard.
  return(
      <ExpressionAppContainer>
        <NavigationBar title="Expression Matters"></NavigationBar>
          <BodyContainer>
          <ExpressionDashboardProvider>
            <SideBar isOpen={isSideBarOpen()}>
                <ExpressionSideTabs></ExpressionSideTabs>
            </SideBar>
            <WorkspaceContainer isOpen={isSideBarOpen()}>
              <DashboardTabsContainer>
                <ExpressionDashboard/>
              </DashboardTabsContainer>
              <FooterContainer>
                  <ExpressionFooterTabs/>
               </FooterContainer>
            </WorkspaceContainer>
          </ExpressionDashboardProvider>
        </BodyContainer>
        <ErrorMessageToast messages={messages}></ErrorMessageToast>
      </ExpressionAppContainer>

  );
}

const mapStateToProps = (state: ExpressionAppState) => ({
  expressionGroups: state.data.expressionGroups,
  messages: state.problems.messages
});
const mapDispatchToProps = (dispatch:Dispatch)=>({
  setExpressionGroups: (expressionGroups: ExpressionGroup[])=>dispatch(expressionActionCreators.setExpressionGroups(expressionGroups))
});
export default connect(mapStateToProps, mapDispatchToProps)(ExpressionApp);