import { expressionMessageReducers, expressionReducers } from "ExpressionAppReducers/expressionReducers";
import { workspaceReducers } from "ExpressionAppReducers/workspaceReducers";
import { ExpressionMessageState, ExpressionState, ExpressionUIState } from "ExpressionAppTypes/expressionType";
 // import { sideBarReducers } from "olapApp/store/reducers/sidebarReducer";
// import { SidebarState } from "olapApp/store/types/sidebarType";
import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";

export interface ExpressionAppState {
  // sidebar: SidebarState;
  data: ExpressionState;
  workspace: ExpressionUIState;
  problems: ExpressionMessageState;
}

const expressionAppReducers = combineReducers({
  // sidebar: sideBarReducers,
  data: expressionReducers,
  workspace: workspaceReducers,
  problems: expressionMessageReducers
});

export const expressionAppStore
 = configureStore({reducer:expressionAppReducers, devTools: process.env.NODE_ENV === "development"});