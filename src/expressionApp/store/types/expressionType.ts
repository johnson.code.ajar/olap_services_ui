import { ExpressionDashboard, ExpressionOutputTab } from "expressionApp/models/workspace";
import { ExpressionGroup, ExpressionMessage, GraphOutput } from "ExpressionAppModels/expression";

export type ExpressionState = {
  expressionGroups: ExpressionGroup[];
  expressionOutputs: {[key:string]:GraphOutput};
};

export type ExpressionUIState = {
  dashboard: ExpressionDashboard;
  outputTab: ExpressionOutputTab;
}

export type ExpressionMessageState = {
  messages: ExpressionMessage[];
}

export enum ExpressionEditorAction {
  SET_EXPRESSION_SEGMENTS,
  SET_ACTIVE_SEGMENT,
  DELETE_SEGMENT,
  ADD_SEGMENT,
  UPDATE_SEGMENT,
  SET_SEGMENT_OUTPUT,
  RUN_SEGMENT,
  SET_EXPRESSION,
  RUN_EXPRESSION,
  SAVE_EXPRESSION,
  SET_MESSAGES,
} 