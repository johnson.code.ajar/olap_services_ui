import { ExpressionGroup, GraphOutput } from "ExpressionAppModels/expression";
import { useSelector } from "react-redux";
import { ExpressionAppState } from "./index";

const exprGraphOutputs = useSelector<ExpressionAppState, {[key:string]:GraphOutput}>(appState=>appState.data.expressionOutputs);

const exprGraphs= useSelector<ExpressionAppState, ExpressionGroup[]>(appState=>appState.data.expressionGroups);
// createSelector are avaliable in reselect, need installation.
// const selectExprGraphOutput = createSelector(exprGraphOutputs, (exprGraphId:string)=>{
//   return exprGraphOutputs[exprGraphId];
// })