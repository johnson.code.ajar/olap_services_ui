import { Expression, ExpressionGroup, ExpressionMessage, ExpressionSegment, GraphOutput } from "ExpressionAppModels/expression";
import { ExpressionEditorAction } from "ExpressionAppStore/types/expressionType";
import { ExpressionEditorTab, ExpressionOutputTab } from "expressionApp/models/workspace";
import { Action, AnyAction } from "redux";
import {
  ADD_EXPRESSION,
  ADD_EXPRESSION_TAB,
  CLOSE_EXPRESSION_OUTPUT_TAB,
  DELETE_EXPRESSION,
  OPEN_EXPRESSION_OUTPUT_TAB,
  SET_ERROR_MESSAGE,
  SET_EXPRESSIONS,
  SET_EXPRESSION_GROUPS,
  SET_EXPRESSION_OUTPUT,
  UPDATE_EXPRESSION,
  UPDATE_EXPRESSION_GROUP
} from "./actionTypes";

interface setExpressionAction  extends Action {
  type: typeof SET_EXPRESSIONS;
  payload: {expressions: Expression, groupId: number};
}

interface setExpressionGroupsAction extends Action {
  type: typeof SET_EXPRESSION_GROUPS;
  payload: {expressionGroups: ExpressionGroup[]}
}

interface addExpressionTabAction extends Action {
  type: typeof ADD_EXPRESSION_TAB;
  payload: {expressionTab: ExpressionEditorTab}
}

interface setErrorMessage extends Action {
  type: typeof SET_ERROR_MESSAGE;
  payload: {message: string}
}

interface updateExpressionGroupActoin extends Action {
  type: typeof UPDATE_EXPRESSION_GROUP;
  payload: {expressionGroup: ExpressionGroup}
}

interface updateExpressionAction extends Action {
  type: typeof UPDATE_EXPRESSION;
  payload: {expression: Expression}
}

interface addExpression extends Action {
  type: typeof ADD_EXPRESSION;
  payload: {expression: Expression}
}

interface deleteExpression extends Action {
  type: typeof DELETE_EXPRESSION;
  payload: {expression: Expression}
}

interface setExpressionOutput extends Action {
  type: typeof SET_EXPRESSION_OUTPUT;
  payload: {id: string; expressionOutput: GraphOutput}
}

interface  openExpressionOutputTab extends Action {
  type: typeof OPEN_EXPRESSION_OUTPUT_TAB;
  payload: ExpressionOutputTab;
}

interface closeExpressionOutputTab extends Action {
  type: typeof CLOSE_EXPRESSION_OUTPUT_TAB;
  payload: ExpressionOutputTab;
}

export type ExpressionActions = AnyAction |
  setExpressionAction |
  setExpressionGroupsAction |
  addExpression |
  deleteExpression |
  setExpressionOutput ;


export type ExpressionMessageActions = AnyAction | setErrorMessage;

export const expressionActionCreators = {
  setExpression: (expression:Expression, groupId: number)=>({
    type: SET_EXPRESSIONS,
    payload: {expression, groupId}
  }),
  updateExpression: (expression: Expression)=>({
    type: UPDATE_EXPRESSION,
    payload: {expression}
  }),
  addExpression: (expression: Expression)=>({
    type: ADD_EXPRESSION,
    payload: {expression}
  }),
  deleteExpression: (expression: Expression)=>({
    type: DELETE_EXPRESSION,
    payload: {expression}
  }),
  setExpressionGroups: (expressionGroups: ExpressionGroup[]) => ({
    type: SET_EXPRESSION_GROUPS,
    payload: {expressionGroups}
  }),
  updateExpressionGroup: (expressionGroup: ExpressionGroup) => ({
    type: UPDATE_EXPRESSION_GROUP,
    payload: {expressionGroup}
  }),
  setExpressionOutput: (id: string, expressionOutput: GraphOutput) => ({
    type: SET_EXPRESSION_OUTPUT,
    payload: {id, expressionOutput}
  })
}

export const errorMessageActionCreators = {
  setErrorMessage: (message: string) => ({
    type: SET_ERROR_MESSAGE,
    payload: {message}
  })
}

// Tidy up is required to merge the actions in ExpressionActions
export type ExpressionEditorActions = |{
  type: ExpressionEditorAction.SET_ACTIVE_SEGMENT,
  payload: {segment:ExpressionSegment}
} | {
  type: ExpressionEditorAction.DELETE_SEGMENT,
  payload: {segment:ExpressionSegment}
} |{
  type: ExpressionEditorAction.ADD_SEGMENT,
  payload: {segment: ExpressionSegment}
}|{
  type: ExpressionEditorAction.UPDATE_SEGMENT,
  payload: {segment: ExpressionSegment}
} | {
  type: ExpressionEditorAction.SET_SEGMENT_OUTPUT,
  payload: {segment: ExpressionSegment}
} | {
  type: ExpressionEditorAction.RUN_SEGMENT,
  payload: {segment: ExpressionSegment}
} | {
  type: ExpressionEditorAction.RUN_EXPRESSION,
  payload: {expression: Expression}
} | {
  type: ExpressionEditorAction.SAVE_EXPRESSION,
  payload: {expression: Expression}
} | {
  type: ExpressionEditorAction.SET_EXPRESSION,
  payload: {expression: Expression}
} | {
  type: ExpressionEditorAction.SET_EXPRESSION_SEGMENTS,
  payload: {segments: ExpressionSegment[]}
} | {
  type: ExpressionEditorAction.SET_MESSAGES,
  payload: {messages: ExpressionMessage[]}
};


