import { sidebarActions } from "olapApp/store/actions/sidebarAction";
import { ExpressionActions, errorMessageActionCreators, expressionActionCreators } from "./expressionAction";
import { ExpressionDashboardActions, ExpressionOutputTabActions, dashboardActionCreators, expressionOutputTabActionCreators } from "./workspaceAction";
export type ExpressionAppActions = sidebarActions |
  ExpressionActions |
  ExpressionDashboardActions |
  ExpressionOutputTabActions;

export const expressionAppActionCreators = {
  expressions: expressionActionCreators,
  dashboards: dashboardActionCreators,
  outputTab: expressionOutputTabActionCreators,
  errors: errorMessageActionCreators,
}