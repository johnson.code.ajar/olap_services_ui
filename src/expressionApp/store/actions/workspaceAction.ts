import { ExpressionEditorTab, ExpressionOutputTab } from "expressionApp/models/workspace";
import {
  ADD_EXPRESSION_TAB,
  CLOSE_EXPRESSION_OUTPUT_TAB,
  OPEN_EXPRESSION_OUTPUT_TAB,
  SET_ACTIVE_TAB,
} from "./actionTypes";

import { Action } from "redux";

interface addExpressionTabAction extends Action {
  type: typeof ADD_EXPRESSION_TAB;
  payload: {expressionTab: ExpressionEditorTab}
}

interface  openExpressionOutputTab extends Action {
  type: typeof OPEN_EXPRESSION_OUTPUT_TAB;
  payload: ExpressionOutputTab;
}

interface closeExpressionOutputTab extends Action {
  type: typeof CLOSE_EXPRESSION_OUTPUT_TAB;
  payload: ExpressionOutputTab;
}

interface setActiveTab extends Action {
  type: typeof SET_ACTIVE_TAB;
  payload: string;
}

export type ExpressionDashboardActions = | addExpressionTabAction | setActiveTab;
export type ExpressionOutputTabActions =  | openExpressionOutputTab | closeExpressionOutputTab;

export const dashboardActionCreators = {
  addDashboardTab: (expressionTab: ExpressionEditorTab) =>({
    type: ADD_EXPRESSION_TAB,
    payload: {expressionTab}
  }),
  setActiveTab: (activeTabId: string)=>({
    type: SET_ACTIVE_TAB,
    payload: {activeTabId}
  })
};

export const expressionOutputTabActionCreators = {
  openExpressionOutputTab: (tab: ExpressionOutputTab) =>({
    type: OPEN_EXPRESSION_OUTPUT_TAB,
    payload: tab
  }),
  closeExpressionOutputTab: (tab: ExpressionOutputTab) =>({
    type: CLOSE_EXPRESSION_OUTPUT_TAB,
    payload: {
      ...tab,
      isOpen: false 
    }
  })
}