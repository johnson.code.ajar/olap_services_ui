import { ExpressionActions, ExpressionMessageActions } from "ExpressionAppActions/expressionAction";
import { Expression, ExpressionGroup, GraphOutput } from "ExpressionAppModels/expression";
import { ExpressionMessageState, ExpressionState, ExpressionUIState } from "ExpressionAppTypes/expressionType";
import {
  ADD_EXPRESSION,
  ADD_EXPRESSION_GROUP,
  DELETE_EXPRESSION,
  DELETE_EXPRESSION_GROUP,
  GET_EXPRESSION_GROUPS,
  SET_ERROR_MESSAGE,
  SET_EXPRESSIONS,
  SET_EXPRESSION_GROUPS,
  SET_EXPRESSION_OUTPUT,
  UPDATE_EXPRESSION,
  UPDATE_EXPRESSION_GROUP
} from "../actions/actionTypes";

// TODO: Remove the expressions as they would be in expressionGroups.
const initialExpressionState: ExpressionState = {
  expressionGroups:[],
  expressionOutputs:{}
}

const initialExpressionUIState: ExpressionUIState = {
  dashboard: {
    tabs:[],
    activeTabId:''
  },
  outputTab: {
    expressionId: "",
    isOpen: false,
  }
}

const initialExpressionMessageState: ExpressionMessageState= {
  messages: []
}

const addExpression = (state: ExpressionState, expression: Expression) => {
  const changedExpressionGroup: ExpressionGroup | undefined = state.expressionGroups.find(g=>g.id === expression.groupId);
  if (changedExpressionGroup) {
    const expressions = [...changedExpressionGroup.expressions, expression];
    const partialState = {expressionGroups:state.expressionGroups.map(g=>{
      if(g.id === changedExpressionGroup.id) {
        return {...changedExpressionGroup, expressions:expressions};
      }
      return g;
    })
    }
    return {...state, ...partialState};
  }
}

const deleteExpression = (state: ExpressionState, expression: Expression) => {
  const currentExpressions = state.expressionGroups.find(g=>g.id === expression.groupId)?.expressions;
  if(currentExpressions) {
    const reducedExpressions = currentExpressions?.filter(e=>e.id !== expression.id);
    const partialState = {
      expressionGroups: state.expressionGroups.map(g=>{
        if(g.id === expression.groupId) {
          return {...g, expressions: reducedExpressions}
        }
        return g;
      })
    };
    return {...state, ...partialState};
  }
}

const setExpressionOutput = (state: ExpressionState,id: string, expressionOutput: GraphOutput) => {
  const expressionOutputs = state.expressionOutputs;
  expressionOutputs[id] = expressionOutput;
  const partialState = {
    expressionOutputs
  }
  return {...state, partialState}
}

export const expressionReducers = (state: ExpressionState = initialExpressionState, action: ExpressionActions) => {
  let partialState:ExpressionState|undefined;
  switch(action.type) {
    case SET_EXPRESSIONS:
      console.log("Set expression Groups.........")
      const group = state.expressionGroups.find(g=>g.id == action.payload.groupId);
      if(group) {
        const groups =  state.expressionGroups.map(g=>{
          if(g.id === action.payload.groupId){
            return {
              ...group,
              expressions: action.payload.expressions
            };
          }
          return g;
        }) 
        partialState= {
          ...state,
          expressionGroups: groups
        }
      } else {
        new Error("Cannot find the specified groupId");
      }
    break;
    case ADD_EXPRESSION_GROUP:
      partialState = {
        ...state,
        expressionGroups: [...state.expressionGroups, action.payload]
      }
    break;
    case SET_EXPRESSION_GROUPS:
      partialState={
        ...state,
        expressionGroups: action.payload.expressionGroups
      }
    break;
    case UPDATE_EXPRESSION_GROUP:
      partialState = {
        ...state,
       expressionGroups: state.expressionGroups.map(g=>{
        if(g.id === action.payload.expressionGroup.id) {
          return action.payload.expressionGroup;
        }
        return g;
      })}
    break;
    case GET_EXPRESSION_GROUPS:
      partialState={
        ...state,
        expressionGroups: state.expressionGroups
      }
    break;
    case DELETE_EXPRESSION_GROUP:
      partialState = {
        ...state,
        expressionGroups: state.expressionGroups.filter(e=>e.id !== action.payload.id),
      }
    break;
    case ADD_EXPRESSION:
      partialState = addExpression(state, action.payload.expression);
    break;
    case DELETE_EXPRESSION:
      partialState = deleteExpression(state, action.payload.expression);
    break;
    case SET_EXPRESSION_OUTPUT:
      partialState = setExpressionOutput(state, action.payload.id, action.payload.expressionOutput);
    break;
    case UPDATE_EXPRESSION:
      const expressionGroup:ExpressionGroup | undefined = state.expressionGroups.find(g=>g.id === action.payload.expression.groupId);
      if(!expressionGroup) {
        partialState={
          ...state,
          expressionGroups:state.expressionGroups
        }
      } else {
        const expressions:Expression[] = expressionGroup.expressions.map(e=>{
          if(action.payload.expression.id === e.id) {
            return action.payload.expression;
          }
          return e;
        });
        const expressionGroups:ExpressionGroup[] = state.expressionGroups.map(g=>{
          if(g.id === action.payload.expression.groupId) {
            return {...expressionGroup, expressions};
          }
          return g;
        });
        partialState={
          ...state,
          expressionGroups
        }
      }
    break;
  }
  return partialState ? {...state, ...partialState}: state;
}

// TODO: classify messages based on source.
export const expressionMessageReducers = (state: ExpressionMessageState = initialExpressionMessageState, action: ExpressionMessageActions) => {
  let partialState!: ExpressionMessageState;
  switch(action.type) {
    case SET_ERROR_MESSAGE:
      const currentMessages = state.messages;
      currentMessages.push({
        error: action.payload.message
      })
      partialState = {
        messages: currentMessages
      }
    break;
  }
  return partialState ? {...state, ...partialState}: state;
}