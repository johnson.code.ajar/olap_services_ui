import { ExpressionDashboardActions, ExpressionOutputTabActions } from "ExpressionAppActions/workspaceAction";
import { ExpressionDashboard, ExpressionOutputTab } from "ExpressionAppModels/workspace";
import { combineReducers } from "redux";
import {
  ADD_EXPRESSION_TAB,
  CLOSE_EXPRESSION_OUTPUT_TAB,
  OPEN_EXPRESSION_OUTPUT_TAB,
  SET_ACTIVE_TAB
} from "../actions/actionTypes";

export const expressionDashboardReducers = (state: ExpressionDashboard = {tabs:[], activeTabId:''}, action: ExpressionDashboardActions) => {
  let partialState: ExpressionDashboard | undefined;
  switch(action.type) {
    case ADD_EXPRESSION_TAB:
      const tabs = state.tabs;
      tabs.push(action.payload.expressionTab);
      partialState={
        ...state,
        tabs
      }
    break;
    case SET_ACTIVE_TAB:
      partialState = {
        ...state,
        activeTabId:action.payload
      }
    break;
  }
  return partialState ? {...state, ...partialState}: state;
}

export const expressionOutputTabReducers = (state: ExpressionOutputTab = {expressionId:'', isOpen:false}, action: ExpressionOutputTabActions)=>{
  let partialState: ExpressionOutputTab | undefined;
  switch(action.type) {
    case OPEN_EXPRESSION_OUTPUT_TAB:
      partialState = action.payload
    break;
    case CLOSE_EXPRESSION_OUTPUT_TAB:
      partialState = action.payload
    break;
  }
  return partialState ? {...state, ...partialState}: state; 
}

export const workspaceReducers = combineReducers({
  dashboard: expressionDashboardReducers,
  outputTab: expressionOutputTabReducers
});