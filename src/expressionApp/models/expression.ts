import { Message } from "Components/toasts/errorMessageToast";

export interface ExpressionSegment {
  id: number;
  expression: string;
  expressionId: string;
  modifiedAt: Date;
  createdAt: Date;
  outputs: {[key:string]:ExpressionOutput};
}
export interface Expression {
  id: string;
  name: string;
  modifiedAt: Date;
  createdAt: Date;
  groupId: string;
  segments:ExpressionSegment[];
}

export interface ExpressionGroup {
  id: string;
  name: string;
  description: string;
  author: string;
  expressions: Expression[];
  childGroups: ExpressionGroup[];
}

type OutputType = number | number[] | string | string[];
export interface ExpressionOutput {
  type: string;
  value: OutputType;
  isScalar: boolean;
}

type ExpressionTreeOutput = {
  output: ExpressionOutput;
}

export interface ExpressionTree {
  name:string;
  output: ExpressionTreeOutput;
  rpnExpression: string; 
} 
export interface GraphOutput {
  id: string;
  exprOutputs: {[key:string]: ExpressionOutput};
  exprTrees: ExpressionTree[];
}

export interface ExpressionMessage extends Message{
  
} 