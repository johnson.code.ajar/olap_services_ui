import { Message } from "Components/toasts/errorMessageToast";
export interface ExpressionEditorTab {
  id: string;
  order: number;
  name: string;
  type: string;
  messages?: TabMessage[];
}
export interface ExpressionDashboard {
  tabs: ExpressionEditorTab[];
  activeTabId: string;
}


export interface ExpressionOutputTab {
  isOpen: boolean;
  expressionId: string;
}

export interface TabMessage extends Message {

}