import { Nav, NavItem, Tab } from "react-bootstrap";

import styled from "styled-components";

export const ExpressionSideBarContainer = styled(Tab.Container)`

`

export const ExpressionNavItem = styled(NavItem)`
display: inline-block;
width: 100%;
text-align: center;
padding: 0px;
background: rgb(232, 245, 253);
.nav-link {
  width: 100%;
  margin: 0 auto;
} 
`;

export const ExpressionSideBarContent = styled(Tab.Content)`
width: 100%;
height: 90%;
display: inline-block;
block-size: 90%;
`

export const ExpressionSideBarTabs = styled(Nav)`
display: inline-block;
//height: 100%;
width: 100%;
block-size:10%;
border-top: 3px solid black;
border-radius:6px;
align-items: top;
`