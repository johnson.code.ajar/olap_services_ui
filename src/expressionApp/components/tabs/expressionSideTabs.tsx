import ExpressionTree from "ExpressionAppComponents/tree/expressionTree";
import { ExpressionGroup } from "ExpressionAppModels/expression";
import expressionService from "ExpressionAppServices/expressionService";
import { expressionActionCreators } from "ExpressionAppStore/actions/expressionAction";
import { ExpressionAppState } from "ExpressionAppStore/index";
import React, { useEffect } from "react";
import { Nav, Tab } from "react-bootstrap";
import { connect, useDispatch } from "react-redux";
import { Dispatch } from "redux";
import { ExpressionNavItem, ExpressionSideBarContainer, ExpressionSideBarContent, ExpressionSideBarTabs } from "./expressionSideTabs.style";
interface Props {
  expressionGroups: ExpressionGroup[];
  setExpressionGroups: (expressionGroups: ExpressionGroup[])=>any;
}
const ExpressionSideTabs = ({expressionGroups, setExpressionGroups}:Props) => {
  const dispatch:Dispatch = useDispatch();
  useEffect(()=>{
    const getExpressionGroups = async () => await expressionService.getExpressionGroups(dispatch);
    getExpressionGroups().then(expressionGroups=> {
      setExpressionGroups(expressionGroups); 
    });
  },[]);

  return(<ExpressionSideBarContainer id="expression-sidebar-container">
      <ExpressionSideBarContent>
        <Tab.Pane eventKey="Script">
          <ExpressionTree expressionGroups={expressionGroups}></ExpressionTree>
        </Tab.Pane>
        <Tab.Pane eventKey="Data">
          <div>Data</div>
        </Tab.Pane>
        <Tab.Pane eventKey="Chart">
          <div>Chart</div>
        </Tab.Pane>
      </ExpressionSideBarContent>
      <ExpressionSideBarTabs variant="pills" defaultActiveKey="Function">
        <ExpressionNavItem><Nav.Link eventKey="Script">Script</Nav.Link></ExpressionNavItem>
        <ExpressionNavItem><Nav.Link eventKey="Data">Data</Nav.Link></ExpressionNavItem>
        <ExpressionNavItem><Nav.Link eventKey="Chart">Chart</Nav.Link></ExpressionNavItem>
      </ExpressionSideBarTabs>
  </ExpressionSideBarContainer>);
}

const mapStateToProps = (state: ExpressionAppState) => ({
  expressionGroups: state.data.expressionGroups
});
const mapDispatchToProps = (dispatch: Dispatch) => ({
  setExpressionGroups: (expressionGroups: ExpressionGroup[])=>dispatch(expressionActionCreators.setExpressionGroups(expressionGroups))
});
export default connect(mapStateToProps, mapDispatchToProps)(ExpressionSideTabs);