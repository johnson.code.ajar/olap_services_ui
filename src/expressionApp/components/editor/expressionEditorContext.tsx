import { Expression, ExpressionMessage, ExpressionSegment } from "ExpressionAppModels/expression";
import { ExpressionEditorActions } from "ExpressionAppStore/actions/expressionAction";
import { ExpressionEditorAction } from "ExpressionAppStore/types/expressionType";
import React, { Dispatch, createContext, useReducer } from "react";

interface ExpressionEditorState {
  expression: Expression;
  activeSegment?: ExpressionSegment;
  messages:ExpressionMessage[];
}

const initExpressionEditorState:ExpressionEditorState = {
  expression:{
    name: "",
    id: "",
    modifiedAt: new Date(),
    createdAt: new Date(),
    groupId: "",
    segments: []
  },
  activeSegment:{
    id: 0,
    expression: "",
    expressionId: "",
    modifiedAt: new Date(),
    createdAt: new Date(),
    outputs:{}
  },
  messages:[]
}

const expressionReducers = (state:ExpressionEditorState = initExpressionEditorState, action: ExpressionEditorActions)=>{
  let initialState:ExpressionEditorState|undefined;
  switch(action.type) {
    case ExpressionEditorAction.ADD_SEGMENT:
      initialState = {
        ...state,
        expression: {
          ...state.expression,
          segments: [...state.expression.segments, action.payload.segment]
        }
      }
    break;
    case ExpressionEditorAction.DELETE_SEGMENT:
      const remainingSegments = state.expression.segments.filter(seg=>seg.id !== action.payload.segment.id);
      initialState = {
        ...state,
        expression: {
          ...state.expression,
          segments: remainingSegments
        }
      }
    break;
    case ExpressionEditorAction.UPDATE_SEGMENT:
      const updatedSegments = state.expression.segments.map(seg=>{
        if(seg.id === action.payload.segment.id){
          return action.payload.segment
        }
        return seg;
      });
      initialState = {
        ...state,
        expression: {
          ...state.expression,
          segments: updatedSegments
        }
      }
    break;
    case ExpressionEditorAction.RUN_SEGMENT:
      const runSegments = state.expression.segments.map(seg=>{
        if(seg.id == action.payload.segment.id){
          return action.payload.segment
        }
        return seg;
      });
      initialState = {
        ...state,
        expression: {
          ...state.expression,
          segments: runSegments
        }
      }
    break;
    case ExpressionEditorAction.SET_ACTIVE_SEGMENT:
      console.log(action);
      initialState = {
        ...state,
        activeSegment: action.payload.segment
      }
    break;
    case ExpressionEditorAction.SET_SEGMENT_OUTPUT:
    break;
    case ExpressionEditorAction.RUN_EXPRESSION:
    break;
    case ExpressionEditorAction.SAVE_EXPRESSION:
    break;
    case ExpressionEditorAction.SET_EXPRESSION:
      console.log("Set expression from expression editor...");
      initialState = {
        ...state,
        expression: action.payload.expression,
        activeSegment: action.payload.expression.segments ? action.payload.expression.segments[0]:undefined
      }
    break;
    case ExpressionEditorAction.SET_EXPRESSION_SEGMENTS:
      initialState = {
        ...state,
        expression: {
          ...state.expression,
          segments: action.payload.segments
        }
      }
    break;
    case ExpressionEditorAction.SET_MESSAGES:
      initialState = {
        ...state,
        messages:action.payload.messages
      }
    break;
  }
  return {...state, ...initialState};
}

type ExpressionContextType = {
  expressionState: ExpressionEditorState;
  expressionDispatch: Dispatch<ExpressionEditorActions> 
}

const ExpressionEditorContext = createContext<ExpressionContextType>({
  expressionState:initExpressionEditorState,
  expressionDispatch:()=>null
});

const ExpressionEditorProvider:React.FC<React.PropsWithChildren>=({children})=>{
  const [expressionState, expressionDispatch] = useReducer(expressionReducers, initExpressionEditorState);
  return(
    <ExpressionEditorContext.Provider value={{expressionState, expressionDispatch}}>{children}</ExpressionEditorContext.Provider>
  );
}

export { ExpressionEditorContext, ExpressionEditorProvider };

