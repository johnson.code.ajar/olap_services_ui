
import { faListAlt, faPlay, faPlusSquare, faSave } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  ExpressionSegmentContainer,
  ExpressionSegmentToolbarContainer,
  ExpressionTextEditor, ExpressionToolbarButton
} from "ExpressionAppComponents/editor/expressionEditor.style";
import { ExpressionEditorContext } from "ExpressionAppComponents/editor/expressionEditorContext";
import { ExpressionSegment } from "ExpressionAppModels/expression";
import expressionService from "ExpressionAppServices/expressionService";
import React, { useCallback, useContext, useState } from "react";
import ReactJson from "react-json-view";

// TODO: Use react-simple-code-editor and prismjs to support syntax
// highlighting for my scripting language.
// Use json style output display instead of text.
// https://github.com/mac-s-g/react-json-view?tab=readme-ov-file
export const ExpressionSegmentEditor = (props:ExpressionSegment)=> {
  const [segment, setSegment] = useState<ExpressionSegment>(props);
  const {expressionState, expressionDispatch} = useContext(ExpressionEditorContext);
  const handleChange = useCallback((e:React.ChangeEvent<HTMLTextAreaElement>)=>{
    if(!segment) {
      return;
    }
    console.log(e.target.value);
    setSegment({
      ...segment,
      expression: e.target.value,
    });
  }, [segment]);
  
  const saveSegment = useCallback(()=>{
    const saveSegment = async (changedSegment:ExpressionSegment)=> await expressionService.saveExpressionSegment(expressionDispatch, changedSegment);
    saveSegment({...segment,
      modifiedAt: new Date()
    });
  },[expressionDispatch, segment]);

  const openSegment = ()=>{
    console.log("Open segment....");
  }

  const addSegment = useCallback(()=>{
    const addSegment = async () => await expressionService.createExpressionSegment(expressionDispatch, props.expressionId);
    addSegment();
  },[expressionDispatch]);

  const runSegment = useCallback(()=>{
    const runSegment = async (toRunSegment: ExpressionSegment)=>await expressionService.runExpressionSegment(expressionDispatch, toRunSegment);
    runSegment(segment).then(
      response=>response && setSegment(response)
    );
    
  },[segment]);
  // TODO: If the expression segment has output include them below the expression segment.
  const SaveExpressionSegment = ()=>{
    return(<ExpressionToolbarButton>
       <FontAwesomeIcon className="fa-light" icon={faSave} onClick={saveSegment}></FontAwesomeIcon>
    </ExpressionToolbarButton>);
  }
  const ViewExpressionSegmentButton = ()=>{
    return(<ExpressionToolbarButton>
      <FontAwesomeIcon className="fa-light" icon={faListAlt} onClick={openSegment}></FontAwesomeIcon>
    </ExpressionToolbarButton>);
  }

  const RunExpressionButton = () => {
    return(
    <ExpressionToolbarButton data-toggle="collapse" onClick={runSegment}>
      <FontAwesomeIcon icon={faPlay}></FontAwesomeIcon>
    </ExpressionToolbarButton>);
  }

  const NewSegmentButton = () =>{
    return(<ExpressionToolbarButton style={{justifyContent:'right'}} onClick={addSegment}>
      <FontAwesomeIcon icon={faPlusSquare}></FontAwesomeIcon>
    </ExpressionToolbarButton>);
  }


  const SegmentOutput = useCallback(({segment}:{segment:ExpressionSegment})=>{
    if(!segment.outputs) {
      return<></>
    }
    //TODO: Handle styling..
    return <ReactJson src={segment.outputs} theme="monokai"></ReactJson>
  },[]);

  return(
  <ExpressionSegmentContainer>
    <ExpressionSegmentToolbarContainer>
      <label style={{fontSize:"12px"}}>[{props.id}]:</label>
      <SaveExpressionSegment></SaveExpressionSegment>
      <ViewExpressionSegmentButton></ViewExpressionSegmentButton>
      <RunExpressionButton></RunExpressionButton>
      <NewSegmentButton></NewSegmentButton>
    </ExpressionSegmentToolbarContainer>
    <div style={{display:"inline-block", width:"100%"}}>
    <ExpressionTextEditor onChange={handleChange} defaultValue={props.expression?props.expression:""}>
    </ExpressionTextEditor>
    <SegmentOutput segment={segment}></SegmentOutput>
    </div>
  </ExpressionSegmentContainer>);
}