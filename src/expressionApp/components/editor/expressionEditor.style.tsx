import styled from 'styled-components';

export const ExpressionEditorContainer = styled.div`
display: inline-block;
height:100%;
width:100%;
border-bottom: 1px solid black;
overflow-y: auto;
overflow-x: hidden;

`;

export const ExpressionSegmentContainer = styled.div`
  display: block;
  min-height: 5em;
  margin-left: 5px;
  margin-right: 5px;
  border-radius:4px;
  margin-top: 10px;
  // &> span {
  //   display:none;
  // }
  :hover {
    border 1px solid rgb(13, 110, 253);
    // &> span {
    //   display:flex;
    //   flex-direction: row;
    // }
  }
  table {
    align-text: center;
  }
  th {
    font-size:10px;
  }
  td {
    font-size:10px;
  }

  .react-json-view {
    font-size:10px;
    font-family: monospace;
    font-weight: normal;
    max-height: 200px;
    overflow: auto;
  }
`


export const ExpressionSegmentToolbarContainer = styled.span`
  flex-direction: column;
  padding-left:4px;
`

export const ExpressionToolbarContainer = styled.div`
margin-right:10px;
border: 1px solid black;
width: 100%;
border-bottom:2px;
display: flex;
flex-direction: row;
`
export const ExpressionTextEditor = styled.textarea`
  display: flex;
  flex-direction: column;
  border:none;
  min-height:75px;
  width: 100%;
  line-height:15px;
  resize: vertical;
  opacity:1;
  background: rgb(255, 255, 255);
  :focus {
    border: solid 1px green;
  }
  padding-left:10px;
  padding-top:2px;
  padding-bottom:2px;
  pageBreakBefore:avoid;
  font-size:14px;
  font-family:Lucida Console, Courier New, monospace;
`
export const ExpressionToolbarButton = styled.button`
margin-right:2px;
border: none;
border-radius:4px;
background: rgb(232, 245, 253);
.svg-inline--fa {
  opacity:0.5;
}
> .fa-play {
  color: red;
 
}
> .fa-floppy-disk, .fa-rectangle-list {
  color: black;
}
:hover {
  background: #0d6efd;
  .svg-inline--fa {
    opacity:1;
  }
  > .fa-floppy-disk, .fa-rectangle-list, .fa-square-plus {
    color: white;
  }
}
`
