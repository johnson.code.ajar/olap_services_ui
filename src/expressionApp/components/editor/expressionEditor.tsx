import { faListAlt, faPlay, faSave } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ExpressionEditorContainer, ExpressionToolbarButton, ExpressionToolbarContainer } from "ExpressionAppComponents/editor/expressionEditor.style";
import { ExpressionEditorContext } from "ExpressionAppComponents/editor/expressionEditorContext";
import { ExpressionSegmentEditor } from "ExpressionAppComponents/editor/expressionSegmentEditor";
import { Expression } from "ExpressionAppModels/expression";
import expressionService from "ExpressionAppServices/expressionService";
import { ExpressionEditorAction } from "ExpressionAppStore/types/expressionType";
import React, { useCallback, useContext, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { DashboardTabContext } from "../dashboard/expressionDashboardContext";
// https://github.com/zenoamaro/react-quill
// https://github.com/react-simple-code-editor/react-simple-code-editor
// https://github.com/react-syntax-highlighter/react-syntax-highlighter
// https://www.commoninja.com/blog/build-code-syntax-highlighter-react#What-Is-Syntax-Highlighting?
export const ExpressionEditor = (props:{expressionId:string;}) => {
  const {state, dashboardDispatch} = useContext(DashboardTabContext);
  const {expressionState, expressionDispatch} = useContext(ExpressionEditorContext);
  const [expression, setExpression] = useState<Expression>();
  const appDispatch = useDispatch();

  useEffect(()=>{
    const getExpression = async () => await expressionService.getExpression(appDispatch, props.expressionId);
    const getExpressionSegments = async ()=> await expressionService.getExpressionSegments(appDispatch, props.expressionId);
    getExpression().then(expr=>{
      expressionDispatch({
        type: ExpressionEditorAction.SET_EXPRESSION,
        payload: {
          expression:expr
        }
      });
      // TODO: Move this service..
      getExpressionSegments().then(segments=>{
        expressionDispatch({
          type: ExpressionEditorAction.SET_EXPRESSION_SEGMENTS,
          payload: {
            segments
          }
        });
      }).catch(e=>{
        dashboardDispatch({
          type: 'SET_TAB_MESSAGES',
          payload: {
            id: props.expressionId,
            messages: [{error:e.message}]
          }
        })
      });
    })
    .catch(e=>{
      dashboardDispatch({
        type: 'SET_TAB_MESSAGES',
        payload: {
          id: props.expressionId,
          messages: [{error:e.message}]
        }
      })
    });

    
    
  },[props.expressionId, setExpression]);
  
  const saveExpression = useCallback(()=>{
    if(!expression) {
      return;
    }
    const saveExpression = async () => await expressionService.updateExpression(appDispatch, expression);
    saveExpression();
  },[expression]);

  const runExpression = useCallback(() => {
    if(!expressionState.expression){
      return;
    }
    const executeExpression = async (expression:Expression) => await expressionService.executeExpression(appDispatch, expression);
    const result = executeExpression(expressionState.expression);
    console.log(result);
  },[expressionState.expression])

  const openOutputTab = ()=>{
    console.log("Open output tab...");
  }

  const RunExpressionButton = () => {
    return(
    <ExpressionToolbarButton data-toggle="collapse" onClick={runExpression}>
      <FontAwesomeIcon icon={faPlay}></FontAwesomeIcon>
    </ExpressionToolbarButton>);
  }

  const SaveExpressionButton = () => {
    return(<ExpressionToolbarButton>
      <FontAwesomeIcon className="fa-light" icon={faSave} onClick={saveExpression}></FontAwesomeIcon>
    </ExpressionToolbarButton>);
  }

  const ViewExpressionOutputButton = () => {
    return (<ExpressionToolbarButton>
      <FontAwesomeIcon icon={faListAlt} onClick={openOutputTab}></FontAwesomeIcon>
      </ExpressionToolbarButton>);
  }
  return(<ExpressionEditorContainer>
      <ExpressionToolbarContainer>
        <SaveExpressionButton/>
        <RunExpressionButton/>
        <ViewExpressionOutputButton/>
      </ExpressionToolbarContainer>
      {expressionState.expression && expressionState.expression.segments &&
      expressionState.expression.segments.map(s=><ExpressionSegmentEditor key={`${expressionState.expression.id}_${s.id}`} {...s}></ExpressionSegmentEditor>)}
  </ExpressionEditorContainer>);
} 