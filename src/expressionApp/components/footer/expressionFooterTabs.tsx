import { GraphOutputContainer } from "ExpressionAppComponents/footer/expressionFooter.style";
import React from "react";
import { Tab, Tabs } from "react-bootstrap";
import ExpressionOutputTabContent from "./expressionOutputTabContent";

interface ExpressionFooterTabsProps {
  name: string;
}
export const ExpressionFooterTabs = () => {
  
  return (
  <GraphOutputContainer>
    <Tabs>
      <Tab eventKey={1} title="Output">
        <ExpressionOutputTabContent/>
      </Tab>
  </Tabs>
  </GraphOutputContainer>);
};