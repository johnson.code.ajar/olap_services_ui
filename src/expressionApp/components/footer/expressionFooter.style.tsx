import styled from "styled-components";
export const GraphOutputContainer = styled.div`
display:block;
max-height:150px;
height:100%;
.nav-link:hover {
  border-top: 3px solid #0d6efd;
  border-left: 1px solid #0d6efd;
  border-right: 1px solid #0d6efd;
}
.nav-link.active {
  background: #0d6efd;
  :hover {
    border-top: 3px solid white;
    border-left: 1px solid white;
    border-right: 1px solid white;
  }
  color: white;
  .fa-plus {
    color: white;
  }
  .fa-xmark {
    color: white;
  }
}

`