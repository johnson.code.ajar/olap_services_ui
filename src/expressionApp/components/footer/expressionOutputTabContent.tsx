import { DashboardTabContext } from "ExpressionAppComponents/dashboard/expressionDashboardContext";
import { GraphOutput } from "ExpressionAppModels/expression";
import { ExpressionAppState } from "ExpressionAppStore/index";
import { ExpressionOutputTab } from "expressionApp/models/workspace";
import React, { useContext } from "react";

import { connect, useSelector } from "react-redux";

interface ExpressionOutputTabProps {
  tab: ExpressionOutputTab;
}
const ExpressionOutputTabContent  = ({tab}:ExpressionOutputTabProps) => {
  const {state, dashboardDispatch} = useContext(DashboardTabContext);
  const outputs:GraphOutput = useSelector<ExpressionAppState, GraphOutput>((appState)=> appState.data.expressionOutputs[tab.expressionId]);
  if(outputs === undefined || outputs.exprOutputs === undefined) {
    return <></>
  }
  return (
    <table className="table">
      <thead>
        <tr>
        <td>Variable</td>
        <td>Value</td>
        </tr>
      </thead>
      <tbody>
      {Object.keys(outputs.exprOutputs).map((variable, index)=>{
        const opVar = outputs.exprOutputs[variable];
        return (<tr style={{height:'5px'}}key={`${variable}_${index}`}>
          <td>{variable}</td>
          <td>{opVar.isScalar?opVar.value:`[${opVar.value}]`}</td>
        </tr>);
      })}
      </tbody>
    </table>
    );
};
ExpressionOutputTabContent.displayName = "ExpressionOutputFooterTab";
const mapStateToProps = (appState: ExpressionAppState)=>{
  return ({
  tab: appState.workspace.outputTab,
})};

export default connect(mapStateToProps)(ExpressionOutputTabContent);
