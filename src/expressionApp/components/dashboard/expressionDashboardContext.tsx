import { ExpressionEditorTab, TabMessage } from "expressionApp/models/workspace";
import React, { Dispatch, createContext, useReducer } from 'react';

interface ExpressionDashboardState {
  tabs: ExpressionEditorTab[];
}

const initDashboardState: ExpressionDashboardState = {
  tabs: []
}

type ExpressionDashboardActions = | {
  type: 'ADD_EXPRESSION_TAB',
  payload: ExpressionEditorTab
} | {
  type: 'CLOSE_EXPRESSION_TAB',
  payload: ExpressionEditorTab
} | {
  type: 'MOVE_EXPRESSION_TAB',
  payload: {
    location:ExpressionEditorTab,
    before: boolean;
    after: boolean;
    tab: ExpressionEditorTab
  }
} | {
  type: 'UPDATE_EXPRESSION_TAB',
  payload: ExpressionEditorTab
} | {
  type: 'SET_TAB_MESSAGES',
  payload: {
    id: string;
    messages: TabMessage[]
  }
};

interface DashboardContextType {
  state: ExpressionDashboardState;
  dashboardDispatch: Dispatch<ExpressionDashboardActions>;
}
const dashboardReducers = (state: ExpressionDashboardState, action: ExpressionDashboardActions) => {
  let partialState: ExpressionDashboardState|undefined;
  switch(action.type) {
    case 'ADD_EXPRESSION_TAB':
      console.log("Add Expression Tab");
      partialState = {
        tabs:[...state.tabs, action.payload]
      }
    break;
    case 'CLOSE_EXPRESSION_TAB':
      partialState = {
        tabs: state.tabs.filter(t=>t.id !== action.payload.id)
      }
    break;
    case 'UPDATE_EXPRESSION_TAB':
      const updatedTabs = state.tabs.map(tab=>{
        if(tab.id === action.payload.id) {
          return action.payload;
        }
        return tab;
      });
      partialState = {
        tabs: updatedTabs
      }
    break;
    case 'MOVE_EXPRESSION_TAB':
      partialState = {
        tabs: []
      }
    break;
    case 'SET_TAB_MESSAGES':
      const tabs = state.tabs.map(t=>{
        if(t.id === action.payload.id) {
          t.messages = action.payload.messages;
        }
        return t;
      })
      partialState = {
        tabs: tabs
      }
    break;
    default:
      throw new Error();
  }
  return partialState ? {...state, ...partialState}: state;
}

const DashboardTabContext = createContext<DashboardContextType>({
  state:initDashboardState,
  dashboardDispatch: ()=>null
});

const ExpressionDashboardProvider: React.FC<React.PropsWithChildren> = ({children}) => {
  const [state, dashboardDispatch] = useReducer(dashboardReducers, initDashboardState);
  return (<DashboardTabContext.Provider value={{state, dashboardDispatch}}>{children}</DashboardTabContext.Provider>);
};

export { DashboardTabContext, ExpressionDashboardProvider };
