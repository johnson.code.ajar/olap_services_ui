import { faClose } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { expressionAppActionCreators } from "ExpressionAppStore/actions";
import { ExpressionEditorTab } from "expressionApp/models/workspace";
import React, { SyntheticEvent, useCallback, useContext } from "react";
import { useDispatch } from "react-redux";
import { DashboardTabContext } from "./expressionDashboardContext";
interface TabCloseProps {
  tab: ExpressionEditorTab;
}
const ExpressionTabTitle = React.forwardRef((props: TabCloseProps, ref) => {
  const tab = props.tab;
  const {state, dashboardDispatch} = useContext(DashboardTabContext);
  const appDispatch = useDispatch();
  const closeTab:React.MouseEventHandler<HTMLAnchorElement> = useCallback((e)=>{    
    dashboardDispatch({
      type: 'CLOSE_EXPRESSION_TAB',
      payload: props.tab
    });
  }, [props.tab]);

  const onTabSelect:React.MouseEventHandler<HTMLDivElement> = useCallback((e:SyntheticEvent) => {
    appDispatch(expressionAppActionCreators.dashboards.setActiveTab(tab.id));
    appDispatch(expressionAppActionCreators.outputTab.openExpressionOutputTab({
      isOpen:true,
      expressionId: tab.id
    }));
  },[props.tab]);

  return (<div onClick={onTabSelect}>
    {tab.name}
    <a style={{paddingLeft:'10px'}} href='#' onClick={closeTab}><FontAwesomeIcon icon={faClose}></FontAwesomeIcon></a>
  </div>
  )
});

ExpressionTabTitle.displayName = 'ExpressionTabTitle';
export default ExpressionTabTitle;