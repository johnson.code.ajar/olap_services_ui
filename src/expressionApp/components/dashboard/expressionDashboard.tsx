import ErrorMessageToast from "Components/toasts/errorMessageToast";
import React, { useContext } from "react";
import { Tab } from "react-bootstrap";
import Tabs from "react-bootstrap/Tabs";
import { DashboardTabContext } from "./expressionDashboardContext";
import ExpressionTabContent from "./expressionTabContent";
import ExpressionTabTitle from "./expressionTabTitle";

interface ExpressionDashboardProps {
  name?: string;
}
const ExpressionDashboard = React.forwardRef(({name= "Expression Dashboard"}:ExpressionDashboardProps, ref)=>{
  const {state, } = useContext(DashboardTabContext);
  //const ref = React.createRef();
  // TOOD: Add a dashboard message removal callback.

  return(
  <Tabs>
    {state.tabs.map(t=>
    <Tab id={t.name} key={`${t.name}_${t.id}_${t.order}`} eventKey={`${t.name}_${t.id}_${t.order}`} title={
      <ExpressionTabTitle ref={ref} tab={t}></ExpressionTabTitle>}>
      <ExpressionTabContent ref={ref} tab={t}></ExpressionTabContent>
      {t.messages?<ErrorMessageToast messages={t.messages}></ErrorMessageToast>:<></>}
    </Tab>)}
  
    {/* <Tab eventKey="addTab" title={ <a href="#"><FontAwesomeIcon className="fa-solid" icon={faPlus}></FontAwesomeIcon></a>}>
    </Tab> */}
  </Tabs>
)});
ExpressionDashboard.displayName = "ExpressionDashboard";
export default ExpressionDashboard;