import { ExpressionEditor } from "ExpressionAppComponents/editor/expressionEditor";
import { ExpressionEditorProvider } from "ExpressionAppComponents/editor/expressionEditorContext";
import { ExpressionGroup } from "ExpressionAppModels/expression";
import expressionService from "ExpressionAppServices/expressionService";
import { ExpressionAppState } from "ExpressionAppStore/index";
import { ExpressionEditorTab } from "expressionApp/models/workspace";
import React, { useContext, useEffect, useMemo } from "react";
import { connect, useDispatch } from "react-redux";
import ExpressionGroupTable from "../table/expressionGroupTable";
import { DashboardTabContext } from "./expressionDashboardContext";
interface ExpressionTabContentProps {
  tab: ExpressionEditorTab;
  expressions: ExpressionGroup[];
}
// TODO: Use expressions from appState instead of local state.
const ExpressionTabContent= React.forwardRef((props: ExpressionTabContentProps, ref) => {
  const {state,} = useContext(DashboardTabContext);
  const dispatch = useDispatch();
  const tab = props.tab;
  useEffect(()=>{
    const getExpressions = async ()=> await 
      expressionService.getExpressions(dispatch,tab.id);
    getExpressions();
  },[])

  const groupExpressions = useMemo(()=>{
    return props.expressions.find(e=>e.id === props.tab.id)?.expressions;
  },[props.expressions, tab]);

  return (props.tab.type === 'EXPRESSION_GROUP'?
   <ExpressionGroupTable expressions={groupExpressions}></ExpressionGroupTable>:
   <ExpressionEditorProvider>
      <ExpressionEditor expressionId={tab.id}></ExpressionEditor>
    </ExpressionEditorProvider>
  )
});

ExpressionTabContent.displayName = "ExpressionDashboardTab";
const mapStateToProps = (state: ExpressionAppState) => ({
  expressions: state.data.expressionGroups
});
export default connect(mapStateToProps)(ExpressionTabContent);