import { Expression, ExpressionMessage } from "ExpressionAppModels/expression";
import formatDate from "Utils/dateUtils";
import { ExpressionEditorTab } from "expressionApp/models/workspace";
import React, { useCallback, useContext } from "react";
import { connect } from "react-redux";
import { ExpressionAppState } from "../../store";
import { DashboardTabContext } from "../dashboard/expressionDashboardContext";
const ExpressionGroupTable = (props:{expressions?:Expression[], messages:ExpressionMessage[]}) => {
  const {state, dashboardDispatch} = useContext(DashboardTabContext);

  const ExpressionLink = (props:{expression: Expression})=>{
    const openExpressionTab = useCallback(()=>{
      const tab: ExpressionEditorTab | undefined = state.tabs.find(t=>t.id === props.expression.id);
      if(props.expression === null) {
        return;
      }
      if(tab != null) {
        return;
      }
      dashboardDispatch({
        type: 'ADD_EXPRESSION_TAB',
        payload:{
        id: props.expression.id,
        order:1,
        name: props.expression.name,
        type: "EXPRESSION"
      }});
    },[props.expression, dashboardDispatch]);

    if(props.expression == null) {
      return (<></>)
    }
    return(<a href={`#/${props.expression.id}`} onClick={openExpressionTab} >{props.expression.name}</a>);
  };

  return(
  <table className="table">
  <thead>
    <tr>
      <td>#</td>
      <td>Name</td>
      <td>Created At</td>
      <td>Modified At</td>
    </tr>
  </thead>
  <tbody>
  
  {props.expressions?props.expressions.map(e=>
        <tr key={`${e.name}_${e.groupId}_${e.id}`}>
          <td><input type="checkbox"></input></td>
          <td> <ExpressionLink expression={e}></ExpressionLink></td>
          <td>{formatDate(e.createdAt)}</td>
          <td>{formatDate(e.modifiedAt)}</td>
        </tr>
    ):<></>}
    </tbody>
  </table>);
};

const mapStateToProps = (state: ExpressionAppState) => ({
  messages: state.problems.messages
});

export default connect(mapStateToProps)(ExpressionGroupTable)
