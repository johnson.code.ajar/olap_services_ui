import React, { useCallback, useEffect, useRef, useState } from "react";

interface ExpressionNodeNameProps {
  name: string;
  editing: boolean;
  setEditState: (state:boolean)=>void;
  submit: (name:string)=>void;
  nameClick: (event: React.MouseEvent<HTMLLabelElement>)=>void;
}
export const ExpressionNodeName = ({name, editing, setEditState, submit, nameClick}:ExpressionNodeNameProps) => {
  const nameInputRef = useRef<HTMLInputElement>(null);
  const nameLabelRef = useRef<HTMLLabelElement>(null);
  const [nodeName, setNodeName] = useState(name);
  
  useEffect(()=>{
    if(editing) {
      if(nameInputRef.current){
        nameInputRef.current.focus();
      }
    } else {
      if(nameLabelRef.current) {
        nameLabelRef.current.focus();
      }
    }
  },[editing]);

  const handleNameChange = useCallback((e:React.ChangeEvent<HTMLInputElement>)=>{
    setNodeName(e.target.value);
  }, [setNodeName]);

  const submitNameChange = useCallback((e: React.KeyboardEvent<HTMLInputElement>)=>{
    if(e.key === 'Enter') {
      setEditState(false);
      submit(nodeName);
    } else if (e.key === 'Escape') {
      setEditState(false);
    }
  },[setEditState, nodeName]);
  if(!editing) {
    return <label onDoubleClick={nameClick}>{nodeName}</label>
  }
  if(editing) {
    return <input ref={nameInputRef} value={nodeName} onChange={handleNameChange} onKeyDown={submitNameChange}></input>
  } else {
    return <label onDoubleClick={nameClick}>{nodeName}</label>
  }
}