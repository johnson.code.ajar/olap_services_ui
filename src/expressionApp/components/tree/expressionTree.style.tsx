import styled from "styled-components";

export const ExpressionTreeContainer = styled.div`
  display: flex;
  flex-direction: column;
  width:100%;
  margin:2px;
`
export const ExpressionTreeToolbar = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  `

export const ExpressionTreeNodeContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin:2px;
`;

export const ExpressionSearchBar = styled.div`
display: flex;
flex-direction: row;
margin-right:2px;
> input {
  height: 25px;
  width: 80%;
  order:1;
}
> button {
  width:20%;
  order:2;
  border-radius: 2px;
}
:hover {
  > button {
    background: #0d6efd;
    .icon {
      color: white;
    }
  }
}
`

export const ExpressionToolbarButton = styled.button`
width: 100%;
background: rgb(232, 245, 253);
border: 1px solid black;
`
export const ExpressionNodeContainer = styled.li`
display: flex;
width: 100%;
:hover {
  &> label {
    width: 100%;
    color: #0d6efd;
  }
}
`

export const ExpressionGroupNodeContainer = styled.ul`
display:flex;
flex-direction: column;
padding-inline-start: 5px;
list-style-type:none;
margin: 0px;
padding: 0px;
input {
  width: 100%;
}
&> span {
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
}
:hover {
  &> span {
    color: #0d6efd;
  }
}
`;

