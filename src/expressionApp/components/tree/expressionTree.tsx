import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { ExpressionGroup } from "../../models/expression";
import { ExpressionSearchBar, ExpressionToolbarButton, ExpressionTreeContainer, ExpressionTreeToolbar } from "./expressionTree.style";
import ExpressionTreeNode from "./expressionTreeNode";

// check out react-complex-tree good for hierarchies
// https://github.com/lukasbach/react-complex-tree
// another react tree library but no drag/drop.
// https://dgreene1.github.io/react-accessible-treeview/ 
interface Props {
  expressionGroups: ExpressionGroup[];
}



const ExpressionTree = ({expressionGroups}:Props) => {
  const SearchBar = () => {
    return(
      <ExpressionSearchBar>
      <input type="text"></input>
      <ExpressionToolbarButton>
        <FontAwesomeIcon icon={faSearch} className="icon"></FontAwesomeIcon>
      </ExpressionToolbarButton>
      </ExpressionSearchBar>
    );
  }

  return(<ExpressionTreeContainer>
    <ExpressionTreeToolbar>
    </ExpressionTreeToolbar>
    <SearchBar></SearchBar>
    <ExpressionTreeNode key="expressionRootNode" name="expressionRootNode" groups={expressionGroups}></ExpressionTreeNode>
  </ExpressionTreeContainer>);
}

export default  ExpressionTree;