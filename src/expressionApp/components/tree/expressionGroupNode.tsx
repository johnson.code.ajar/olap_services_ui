import { faAngleDown, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { ContextMenu, MenuItemProp } from "Components/context-menu/contextMenu";
import { ExpressionNode } from "ExpressionAppComponents/tree/expressionNode";
import { ExpressionNodeName } from "ExpressionAppComponents/tree/expressionNodeName";
import { ExpressionGroup } from "ExpressionAppModels/expression";
import expressionService from "ExpressionAppServices/expressionService";
import { ExpressionEditorTab } from "expressionApp/models/workspace";
import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { DashboardTabContext } from "../dashboard/expressionDashboardContext";
import { ExpressionGroupNodeContainer } from "./expressionTree.style";

const ExpressionGroupNode = ({group}:{group:ExpressionGroup}) => {
  const {state, dashboardDispatch} = useContext(DashboardTabContext);
  const [isExpanded, setIsExpanded] = useState(false);
  const [renaming, setRenaming] = useState(false);
  const nameInputRef = useRef<HTMLInputElement>(null);
  const nameLabelRef = useRef<HTMLLabelElement>(null);

  const appDispatch  = useDispatch();
  const doubleClickAction:React.MouseEventHandler<HTMLUListElement> = useCallback((e)=> {
    if(!group.expressions) {
      const getExpressions = async ()=> await expressionService.getExpressions(appDispatch, group.id);
      getExpressions();
    }
    setIsExpanded(!isExpanded);
  },[setIsExpanded, isExpanded, group]);


  const openExpressionGroupTab = useCallback((e:React.MouseEvent<HTMLLabelElement>)=>{
    const tab:ExpressionEditorTab | undefined = state.tabs.find(t=>t.id === group.id);
    if(tab) {
      return;
    }
    dashboardDispatch({
      type:'ADD_EXPRESSION_TAB',
      payload: {
        id: group.id,
        order:1,
        name: group.name,
        type: "EXPRESSION_GROUP"
      }
    })
  },[group, state.tabs]);

  const updateExpressionGroupTab = useCallback((tab: ExpressionEditorTab)=> {
   dashboardDispatch({
    type: 'UPDATE_EXPRESSION_TAB',
    payload: tab
   })
  },[]);
  
  useEffect(()=> { 
    if(renaming) {
      if(nameInputRef.current){
        nameInputRef.current.focus();
      }   
    } else {
      if(nameLabelRef.current) {
        nameLabelRef.current.focus();
      }
    }
  },[renaming]);

  const handleRenaming = useCallback(()=>{
    setRenaming(true); 
  },[setRenaming]);

  const createNewExpressionGroup = useCallback(()=>{
    //TODO: Get the author's name.
    const newExpressionGroup = async ()=> await expressionService.createExpressionGroup(appDispatch);
    newExpressionGroup();
  },[state.tabs]);

  const createNewExpression = useCallback(()=>{
    const createExpression = async () => await expressionService.createExpression(appDispatch, group.id);
    createExpression();
  },[group]);

  const deleteExpressionGroup = useCallback(()=>{
      const deleteExpressionGroup = async () => await expressionService.deleteExpressionGroup(appDispatch, group);
      deleteExpressionGroup();
  },[group]);

  const menuItems:MenuItemProp[] = [{
    name: 'Open',
    action: openExpressionGroupTab
  }, {
    name: 'Rename',
    action: handleRenaming
  }, {
    name: isExpanded ? 'New Expression': 'New Group',
    action: isExpanded ? createNewExpression : createNewExpressionGroup
  }, {
    name: 'Delete',
    action: deleteExpressionGroup
  }];

  const saveNameChange = useCallback((newName:string)=>{
    const updateExpressionGroup = async (changedGroup: ExpressionGroup) => await expressionService.updateExpressionGroup(appDispatch,
      changedGroup);
      updateExpressionGroup({...group, name:newName});
      const tab: ExpressionEditorTab | undefined = state.tabs.find(t=>t.id === group.id);
      if(tab){
        updateExpressionGroupTab({...tab, name: newName});
      }
      setRenaming(false);
  },[setRenaming, group, state.tabs]);

  const Expressions = useMemo(()=>{
    if(!isExpanded) {
      return <></>;
    }
    if(!group.expressions || group.expressions.length === 0) {
      return <></>;
    }
    return (<ul key={`${group.id}_${group.name}_exprs`} style={{listStyleType:'none', paddingInlineStart:'20px'}}>
      {group.expressions.map((e,i)=><ExpressionNode key={`${e.name}_${e.id}_${i}`} expression={e}></ExpressionNode>)}
      </ul>);
  },[group.expressions, isExpanded]);
  
  return(
    <ExpressionGroupNodeContainer key={`expressionGrp_${group.id}`}>
      <span id={`expressionGroup_${group.id}`}>
        <i style={{paddingRight:'5px'}} className="fas rotate" onDoubleClick={doubleClickAction}>
          {isExpanded?<FontAwesomeIcon icon={faAngleDown}></FontAwesomeIcon>:<FontAwesomeIcon icon={faAngleRight}></FontAwesomeIcon>}
        </i>
        <ExpressionNodeName name={group.name} editing={renaming} setEditState={setRenaming} submit={saveNameChange} nameClick={openExpressionGroupTab}></ExpressionNodeName>
      </span>
      {Expressions}
      <ContextMenu targetId={group.id} type="expressionGroup" items={menuItems}></ContextMenu>
    </ExpressionGroupNodeContainer>
  );
}

export default ExpressionGroupNode;