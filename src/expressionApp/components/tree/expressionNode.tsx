import { ContextMenu, MenuItemProp } from "Components/context-menu/contextMenu";
import { DashboardTabContext } from "ExpressionAppComponents/dashboard/expressionDashboardContext";
import { ExpressionNodeName } from "ExpressionAppComponents/tree/expressionNodeName";
import { ExpressionNodeContainer } from "ExpressionAppComponents/tree/expressionTree.style";
import { Expression } from "ExpressionAppModels/expression";
import { ExpressionEditorTab } from "ExpressionAppModels/workspace";
import expressionService from "ExpressionAppServices/expressionService";
import { expressionAppActionCreators } from "ExpressionAppStore/actions/index";
import React, { useCallback, useContext, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
export const ExpressionNode = ({expression}:{expression:Expression}) => {
  const {state, dashboardDispatch} = useContext(DashboardTabContext);
  const [renaming, setRenaming] = useState(false);
  const [tab, setTab] = useState<ExpressionEditorTab>();
  const appDispatch = useDispatch();

  useEffect(()=>{
    setTab({
      id: expression.id,
      order: 1,
      name: expression.name,
      type: 'EXPRESSION'
    });
  },[expression]);
  
  const openExpressionTab = useCallback((e:React.MouseEvent<HTMLLabelElement|HTMLAnchorElement>)=> {
    const currentTab: ExpressionEditorTab | undefined = state.tabs.find(t=>t.id === expression.id);
    if(currentTab) {
      return;
    }
    if(!tab){
      return;
    }
    dashboardDispatch({
      type: 'ADD_EXPRESSION_TAB',
      payload: tab
    });
    //Update the active tab.
    appDispatch(expressionAppActionCreators.dashboards.setActiveTab(tab.id));
  },[expression, state.tabs, tab]);
  const updateExpressionTab = (tab: ExpressionEditorTab) => {
    dashboardDispatch({
      type: 'UPDATE_EXPRESSION_TAB',
      payload: tab
    })
  }

  const handleRenaming = useCallback(()=>{
    setRenaming(true);
  },[setRenaming]);

  const createNewExpression = useCallback(()=>{
    const createExpression = async () => await expressionService.createExpression(appDispatch, expression.groupId);
    createExpression();
  },[expression]);

  const deleteExpression = useCallback(()=>{
    const deleteExpression = async () => await expressionService.deleteExpression(appDispatch, expression);
    deleteExpression();
  },[expression]);

  const executeExpression = useCallback(()=>{
    const executeExpression = async () => await expressionService.executeExpression(appDispatch, expression);
    executeExpression();
  },[expression]);

  const menuItems:MenuItemProp[] = [{
    name: 'Open',
    action: openExpressionTab
  }, {
    name: 'Rename',
    action: handleRenaming
  },{
    name: 'New Expression',
    action: createNewExpression
  },{
    name: 'Delete',
    action: deleteExpression
  },{
    name: "Execute",
    action: executeExpression
  }];

  const saveNameChange = useCallback((newName:string)=>{
    const updateExpression = async (expr: Expression) => await expressionService.updateExpression(appDispatch, expr);
    updateExpression({...expression, name:newName});
    if(tab) {
      setTab({...tab, name: newName});
      updateExpressionTab({...tab, name: newName});
    }
    setRenaming(false);
  },[setRenaming, tab, setTab]);

  return (
    <ExpressionNodeContainer id={`expression_${expression.id}`} key={`${expression.name}_${expression.id}`} >
      <ExpressionNodeName nameClick={openExpressionTab} name={expression.name} editing={renaming} setEditState={setRenaming} submit={saveNameChange}></ExpressionNodeName>
      <ContextMenu targetId={expression.id} type="expression" items={menuItems}></ContextMenu>
    </ExpressionNodeContainer>
  );
}