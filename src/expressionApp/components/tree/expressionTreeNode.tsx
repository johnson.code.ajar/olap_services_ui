import { ExpressionGroup } from "ExpressionAppModels/expression";
import React from "react";
import ExpressionGroupNode from "./expressionGroupNode";
import { ExpressionTreeNodeContainer } from "./expressionTree.style";

const ExpressionTreeNode = ({groups, name}:{groups: ExpressionGroup[]; name:string}) => {

  return(<ExpressionTreeNodeContainer id={name}>
    {groups && groups.map((group,i)=>{
      return <ExpressionGroupNode key={`${group.id}_${group.name}`} group={group}></ExpressionGroupNode>
    })}
  </ExpressionTreeNodeContainer>)

}

export default ExpressionTreeNode;