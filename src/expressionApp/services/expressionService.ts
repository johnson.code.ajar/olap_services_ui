import { Expression, ExpressionGroup, ExpressionSegment, GraphOutput } from "ExpressionAppModels/expression";
import { ExpressionEditorActions } from "ExpressionAppStore/actions/expressionAction";
import { ExpressionEditorAction } from "ExpressionAppStore/types/expressionType";
import axios, { AxiosInstance } from "axios";
import { Dispatch } from "redux";
import { expressionAppActionCreators } from "../store/actions/index";

class ExpressionService {
  private expressionUrl:AxiosInstance = axios.create({
    baseURL: `${process.env.EXPRESSION_API_BASE_URL}/expressions`
  });

  private expressionGroupUrl:AxiosInstance = axios.create({
    baseURL: `${process.env.EXPRESSION_API_BASE_URL}/expression-groups`
  });

  private expressionSegmentUrl:AxiosInstance = axios.create({
    baseURL: `${process.env.EXPRESSION_API_BASE_URL}/expression-segments`
  })
 
  public getaAllExpressions = async () => {
    const expressions: Expression[] = await this.expressionUrl.get("").then(response=>response.data);
    return expressions;
  }

  public getExpression = async (dispatch: Dispatch, id: string) => {
    const expression = await this.expressionUrl.get(`/${id}`)
    .then(response => response.data)
    .catch(error=>{
      throw new Error(`Cannot find expression ${id}`);
    });
    return expression;
  }

  public createExpression = async (dispatch: Dispatch, groupId: string) => {
    const expression = await this.expressionUrl.post(`/create?groupId=${groupId}`)
    .then(response=>
      dispatch(expressionAppActionCreators.expressions.addExpression(response.data)))
    .catch(error=>{
      dispatch({
        type: 'SET_ERROR_MESSAGE',
        payload: {message: `Cannot create expression in group`}
      })
    });
    return expression;
  }

  public deleteExpression = async (dispatch: Dispatch, expression: Expression) => {
    await this.expressionUrl.delete(`/${expression.id}`).then(response=>{
      dispatch(expressionAppActionCreators.expressions.deleteExpression(expression))
    })
    .catch(e=>dispatch({
      type: 'SET_ERROR_MESSAGE',
      payload: {message: `Trouble deleting expression ${expression.name}`}
    }));
  }

  public getExpressions = async (dispatch:Dispatch, groupId: string) => {
    const expressions = await this.expressionUrl.get(`?groupId=${groupId}`)
    .then(response => {
      dispatch({
        type: 'SET_EXPRESSIONS',
        payload: {
          groupId: groupId,
          expressions: response.data
        }
      });  
      return response.data
    })
    .catch(e=>{
      dispatch({
        type: 'SET_ERROR_MESSAGE',
        payload: {message: `Trouble getting expression of group ${groupId}`}
      })
    });
    
    return expressions;
  }

  public getExpressionGroups = async (dispatch: Dispatch) => {
    const expressionGroups = await this.expressionGroupUrl.get("/all")
    .then(response=>response.data).catch(
        e=>{
        dispatch({
          type: 'SET_ERROR_MESSAGE',
          payload: {message:"Trouble loading expression group"}
        })
      });
    return expressionGroups;
  }

  public createExpressionGroup = async(dispatch: Dispatch) => {
    const newExpressionGroup = await this.expressionGroupUrl.post("/create")
    .then(response => {
      dispatch({
        type: 'ADD_EXPRESSION_GROUP',
        payload: response.data
      });
    })
    .catch(e=>{
      dispatch({
        type: 'SET_ERROR_MESSAGE',
        payload: {message: "Cannot create expression group."}
      })
    });
    return newExpressionGroup;
  }

  public deleteExpressionGroup = async (dispatch: Dispatch, group: ExpressionGroup) => {
    await this.expressionGroupUrl.delete(`/${group.id}`)
    .then(response => {
      dispatch({type: 'DELETE_EXPRESSION_GROUP',
      payload: group});
    })
    .catch(e=>{
      dispatch({
        type: 'SET_ERROR_MESSAGE',
        payload: {message: "Cannot delete expression group."}  
      });
    });
  }

  public updateExpressionGroup = async(dispatch: Dispatch, group: ExpressionGroup) => {
    const updatedExpressionGroup = await this.expressionGroupUrl.patch("", group)
    .then(response=>{
      dispatch({
        type: 'UPDATE_EXPRESSION_GROUP',
        payload: {expressionGroup:response.data}
      });
      return response.data;
    })
    .catch(e=>{
      dispatch({
        type: 'SET_ERROR_MESSAGE',
        payload: {message: "Cannot update expression group."}
      });
    })
    return updatedExpressionGroup;
  }

  public updateExpression = async(dispatch: Dispatch, expr: Expression) => {
    const updatedExpression = await this.expressionUrl.patch("", expr)
    .then(response=>{
      dispatch({type:'UPDATE_EXPRESSION', payload: {expression: response.data}})
      return response.data;
    })
    .catch(e=>{
      dispatch({
        type: 'SET_ERROR_MESSAGE',
        payload: {message: "Cannot update expression"}
    });
    });
    return updatedExpression;
  }

  public executeExpression = async(dispatch: Dispatch, expr: Expression): Promise<GraphOutput> => {
    const result = await this.expressionUrl.post(`/${expr.id}/run`)
    .then(response=> {
      dispatch(expressionAppActionCreators.expressions.setExpressionOutput(expr.id, response.data));
      dispatch(expressionAppActionCreators.outputTab.openExpressionOutputTab({
        expressionId:response.data.id,
        isOpen:true
      }));
      return response.data;
    })
    .catch(e=>{
      dispatch({
        type: 'SET_ERROR_MESSAGE',
        payload: {message: "Cannot run expression"}
      });
    });
    return result;
  }

  // Expression Segment service.

  public getExpressionSegments = async(dispatch: Dispatch, id: string) => {
    const expressionSegments = await this.expressionSegmentUrl.get(`/${id}`)
    .then(response=>response.data)
    .catch(error=>{
      throw new Error(`Cannot get segments of expression ${id}`);
    });
    return expressionSegments;
  }

  public createExpressionSegment = async(dispatch: React.Dispatch<ExpressionEditorActions>, expressionId: string) =>{
    const result = await this.expressionSegmentUrl.post(`/${expressionId}`)
    .then(response=>{
        dispatch({
          type: ExpressionEditorAction.ADD_SEGMENT,
          payload:{
            segment:response.data
          }
        })
      }).catch(error=>{
        dispatch({
          type: ExpressionEditorAction.SET_MESSAGES,
          payload:{
            messages:[{
              error: error,
              type: "SEGMENT_ERROR"
            }]
          }
        })
      });
    return result;
  }

  public saveExpressionSegment = async(dispatch: React.Dispatch<ExpressionEditorActions>, segment: ExpressionSegment)=>{
    const result = await this.expressionSegmentUrl.patch("", segment)
    .then(response=>{
      dispatch({
        type: ExpressionEditorAction.UPDATE_SEGMENT,
        payload:{
          segment: response.data
        }
      });
    }).catch(error=>{
      dispatch({
        type: ExpressionEditorAction.SET_MESSAGES,
        payload: {
          messages:[{
            error: error,
            type: "SEGMENT_ERROR"
          }]
        }
      });
    });
  }

  public runExpressionSegment = async(dispatch: React.Dispatch<ExpressionEditorActions>, segment: ExpressionSegment) =>{
    const result:ExpressionSegment|undefined = await this.expressionSegmentUrl.post(`/${segment.expressionId}/${segment.id}/run`, segment)
    .then(response=>{
      const runSegment:ExpressionSegment = {
        ...segment,
        outputs: response.data
      }
      dispatch({
        type: ExpressionEditorAction.RUN_SEGMENT,
        payload: {
          segment:{
            ...segment,
            outputs: response.data
          }
        }
      });
      return runSegment;
    }).catch(error=>{
      dispatch({
        type: ExpressionEditorAction.SET_MESSAGES,
        payload: {
          messages:[{
            error: error,
            type: "SEGMENT_ERROR"
          }]
        }
      })
      return undefined;
    });
    return result;
  }
}


const expressionService = new ExpressionService();
export default expressionService;

