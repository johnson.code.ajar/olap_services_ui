import * as React from "react";
import { createRoot } from "react-dom/client";
import { Provider } from "react-redux";
import Application from "./app/application";
import { expressionAppStore } from "./expressionApp/store/index";
import { olapAppStore, websocketAppStore } from "./olapApp/store";
const appNo = 3;
const getStore = (appNo: number)=>{
	if(appNo === 1) {
		return olapAppStore;
	} else if (appNo === 2) {
		return websocketAppStore;
	} else if(appNo ===3) {
		return expressionAppStore;
	}
	return expressionAppStore;
}
// Removing strict mode to handle react-virtualized.
const rootElement = document.getElementById("root");
if(!rootElement) {
	throw new Error("Cannot find the element by id root.");
}
const root = createRoot(
	rootElement 
);

root.render(	<Provider store = {getStore(appNo)}>
<Application/>
</Provider>
)

