import {LinkList} from "../../src/common/linklist";
import {Node} from "../../src/common/node";
describe("LinkedList testing", () => {
  it("initialize linked list", ()=>{
      let list1:LinkList<number, Node<number>> = new LinkList();
      expect(list1.head()).toBeUndefined()
      expect(list1.tail()).toBeUndefined()
      list1.add(new Node(10));
      expect(list1.head()).toBeDefined()
      expect(list1.tail()).toBeDefined()
      list1.add(new Node(20));
      expect(list1.size()).toBe(2)
  });

  it("remove node from list", () => {
    let list1:LinkList<number, Node<number>> = new LinkList();
    let nodes = [
      new Node(10),
      new Node(20),
      new Node(30),
      new Node(40),
    ]
  
    nodes.forEach((node)=> {
      list1.add(node);
    })
    expect(list1.size()).toBe(4);
    list1.remove(nodes[3]);
    expect(list1.size()).toBe(3)
    list1.remove(nodes[0]);
    expect(list1.size()).toBe(2);
    expect(list1.get(0)!.getValue()).toBe(20)
    expect(list1.get(1)!.getValue()).toBe(30)
  });

  it.only("insert a node value", () => {
    let list1:LinkList<number, Node<number>> = new LinkList();
    let nodes = [
      new Node(10),
      new Node(20),
      new Node(30),
      new Node(40)
    ];
    nodes.forEach(node => {
      list1.add(node);
    })
    list1.set(0, 10.4);
    expect(list1.get(0)!.getValue()).toBe(10.4);
    list1.set(3,40.1);
    expect(list1.get(3)!.getValue()).toBe(40.1);
  });
});