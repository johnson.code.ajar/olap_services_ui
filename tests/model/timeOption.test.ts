import {yearOptions, quarterOptions, QuarterOption, YearOption} from "../../src/model/timeOptionType";
import {PeriodType} from "../../src/model/timeOptionType";
import {Option} from "../../src/model/timeOptionType";
describe("testing time option", ()=> {
  it("test year default option", ()=>{
    expect(yearOptions.default(PeriodType.MONTH)).toBe(yearOptions.options[0])
    expect(yearOptions.default(PeriodType.WEEK)).toBe(yearOptions.options[1])
    expect(yearOptions.default("")).toBe(yearOptions.options[0])
  });

  it("test quarter default option", ()=>{
    expect(quarterOptions.default(PeriodType.MONTH)).toBe(quarterOptions.options[0])
    expect(quarterOptions.default(PeriodType.WEEK)).toBe(quarterOptions.options[1])
    expect(quarterOptions.default("")).toBe(quarterOptions.options[0])
  });

  it.only("get option by id", ()=>{
    expect(yearOptions.option(YearOption.YEAR12MONTH)).toBe(yearOptions.options[0])
    expect(quarterOptions.option(QuarterOption.QUARTER13WEEK)).toBe(quarterOptions.options[1])
  })

});